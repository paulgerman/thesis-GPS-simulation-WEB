<?php
class GeneralException extends \Exception
{
	//Here will be error codes
	const PRODUCT_NOT_FOUND = 1;
	const INVALID_PASSWORD = 2;
	const UNSUPPORTED_TYPE = 3;
	const FILE_NOT_FOUND = 4;
}