<?php
namespace Objects;


class Image extends BaseObject
{

	public static $strObjectName = "image";
	public static $strIndexProp = "image_id";
	public static $strTableName = "images";

	public static $arrProps = array(
		"image_id",
		"image_data",
		"image_name",
		"image_tag",
		"image_date"
	);

	public static $arrPropsEnum = array(
	);

	public static $arrPropsOptional = array(
		"image_name" => "",
	);

	public static $arrPropsReadOnly = array(
		"image_date"
	);

	public static $arrRenderProps = array(
	);

	public $arrValues = array();
}