<?php
namespace Objects;


class ThesisTrack extends BaseObject
{

	public static $strObjectName = "track";
	public static $strIndexProp = "track_id";
	public static $strTableName = "tracks";

	public static $arrProps = array(
		"track_id",
		"user_id",
		"track_data_vertices",
		"track_name",
		"track_length_m",
		"track_data_altitude_probes",
		"track_created_time",
		"track_preview_url"
	);


	public static $arrPropsEnum = array(
	);

	public static $arrPropsOptional = array(
	);

	public static $arrPropsReadOnly = array(
		"track_created_time"
	);

	public static $arrRenderProps = array(
	);
}