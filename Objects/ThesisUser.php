<?php
namespace Objects;


class ThesisUser extends BaseObject
{

	public static $strObjectName = "user";
	public static $strIndexProp = "user_id";
	public static $strTableName = "users";

	public static $arrProps = array(
		"user_id",
		"user_email",
		"user_first_name",
		"user_last_name",
		"user_pic",
		"user_rank",
		"user_password",
		"user_salt",
	);

	public static $arrPropsEnum = array(
		"user_rank" => array("admin", "user")
	);

	public static $arrPropsOptional = array(
		"user_rank" => "user",
		"user_salt" => "",
		"user_password" => "",
        "user_pic" => ""
	);

	public static $arrPropsReadOnly = array(
		"user_rank",
	);

	public static $arrRenderProps = array(
		"user_id" => array(
			"displayName" => "ID",
		),
		"user_email" => array(
			"displayName" => "Email",
			"internalAttributes" => array(
				"required" => true,
				"type" => "email"
			)
		),
		"user_first_name" => array(
			"displayName" => "First name",
			"internalAttributes" => array(
				"required" => true
			)
		),
		"user_last_name" => array(
			"displayName" => "Last name",
			"internalAttributes" => array(
				"required" => true
			)
		),
		"user_pic" => array(
			"displayName" => "Image",
			"internalType" => "Image",
			"type" => "file",
			"maxWidthOrHeight" => 500,
		)
	);

	public $arrValues = array();

}