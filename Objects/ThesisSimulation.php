<?php
namespace Objects;


class ThesisSimulation extends BaseObject
{

	public static $strObjectName = "simulation";
	public static $strIndexProp = "simulation_id";
	public static $strTableName = "simulations";

	public static $arrProps = array(
		"simulation_id",
		"simulation_name",
		"simulation_data",
		"user_id",
		"track_id",
		"simulation_total_time_s",
		"simulation_created_time",
		"simulation_access_code"
	);


	public static $arrPropsEnum = array(
	);

	public static $arrPropsOptional = array(
	);

	public static $arrPropsReadOnly = array(
		"simulation_created_time"
	);

	public static $arrRenderProps = array(
	);
}