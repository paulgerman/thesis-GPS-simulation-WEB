function round(number, precision)
{
	let factor = 10 ** precision;
	let tempNumber = number * factor;
	let roundedTempNumber = Math.round(tempNumber);
	return roundedTempNumber / factor;
}

/**
 *   Snap marker to closest point on a line.
 *
 *   Based on Distance to line example by
 *   Marcelo, maps.forum.nu - http://maps.forum.nu/gm_mouse_dist_to_line.html
 *   Then
 *   @ work of Björn Brala - Swis BV who wrapped the algorithm in a class operating on GMap Objects
 *   And now
 *   Bill Chadwick, who factored the basic algorithm out of the class (removing much intermediate storage of results)
 *       and added distance along line to nearest point calculation
 *   Followed by
 *   Robert Crowe, who ported it to v3 of the Google Maps API and factored out the marker to make it more general.
 *
 *   Usage:
 *
 *   Create the class
 *       let oSnap = new cSnapToRoute();
 *
 *   Initialize the subjects
 *       oSnap.init(oMap, oPolyline);
 *
 **/
function cSnapToRoute()
{

	this.routePoints = Array();
	this.routePixels = Array();
	this._oMap;
	this._oPolyline;

	/**
	 *   @desc Initialize the objects.
	 *   @param Map object
	 *   @param GPolyline object - the 'route'
	 **/
	this.init = function (oMap, oPolyline)
	{
		this._oMap = oMap;
		this._oPolyline = oPolyline;

		this.loadRouteData(); // Load needed data for point calculations
	}

	/**
	 *   @desc internal use only, Load route points into RoutePixel array for calculations, do this whenever zoom changes
	 **/
	this.loadRouteData = function ()
	{
		this.routePixels = new Array();
		let proj = this._oMap.getProjection();
		for (let i = 0; i < this._oPolyline.getPath().getLength(); i++)
		{
			let Px = proj.fromLatLngToPoint(this._oPolyline.getPath().getAt(i));
			this.routePixels.push(Px);
		}
	}

	/**
	 *   @desc Get closest point on route to test point
	 *   @param GLatLng() the test point
	 *   @return new GLatLng();
	 **/
	this.getClosestLatLng = function (latlng)
	{
		let r = this.distanceToLines(latlng);
		let proj = this._oMap.getProjection();
		return proj.fromPointToLatLng(new google.maps.Point(r.x, r.y));
	}

	/**
	 *   @desc Get distance along route in meters of closest point on route to test point
	 *   @param GLatLng() the test point
	 *   @return distance in meters;
	 **/
	this.getDistAlongRoute = function (latlng)
	{
		let r = this.distanceToLines(latlng);
		return this.getDistToLine(r.i, r.fTo);
	}

	/**
	 *   @desc internal use only, gets test point xy and then calls fundamental algorithm
	 **/
	this.distanceToLines = function (thisLatLng)
	{
		let tm = this._oMap;
		let proj = this._oMap.getProjection();
		let thisPx = proj.fromLatLngToPoint(thisLatLng);
		let routePixels = this.routePixels;
		return getClosestPointOnLines(thisPx, routePixels);
	}

	/**
	 *   @desc internal use only, find distance along route to point nearest test point
	 **/
	this.getDistToLine = function (iLine, fTo)
	{

		let routeOverlay = this._oPolyline;
		let d = 0;
		for (let n = 1; n < iLine; n++)
		{
			d += google.maps.geometry.spherical.computeDistanceBetween(routeOverlay.getPath().getAt(n - 1), routeOverlay.getPath().getAt(n));
		}
		d += google.maps.geometry.spherical.computeDistanceBetween(routeOverlay.getPath().getAt(iLine - 1), routeOverlay.getPath().getAt(iLine)) * fTo;

		return d;
	}


}

/* desc Static function. Find point on lines nearest test point
 test point pXy with properties .x and .y
 lines defined by array aXys with nodes having properties .x and .y
 return is object with .x and .y properties and property i indicating nearest segment in aXys
 and property fFrom the fractional distance of the returned point from aXy[i-1]
 and property fTo the fractional distance of the returned point from aXy[i]   */


function getClosestPointOnLines(pXy, aXys)
{

	let minDist;
	let fTo;
	let fFrom;
	let x;
	let y;
	let i;
	let dist;

	if (aXys.length > 1)
	{

		for (let n = 1; n < aXys.length; n++)
		{

			if (aXys[n].x != aXys[n - 1].x)
			{
				let a = (aXys[n].y - aXys[n - 1].y) / (aXys[n].x - aXys[n - 1].x);
				let b = aXys[n].y - a * aXys[n].x;
				dist = Math.abs(a * pXy.x + b - pXy.y) / Math.sqrt(a * a + 1);
			}
			else
				dist = Math.abs(pXy.x - aXys[n].x)

			// length^2 of line segment
			let rl2 = (aXys[n].y - aXys[n - 1].y) ** 2 + (aXys[n].x - aXys[n - 1].x) ** 2;

			// distance^2 of pt to end line segment
			let ln2 = (aXys[n].y - pXy.y) ** 2 + (aXys[n].x - pXy.x) ** 2;

			// distance^2 of pt to begin line segment
			let lnm12 = (aXys[n - 1].y - pXy.y) ** 2 + (aXys[n - 1].x - pXy.x) ** 2;

			// minimum distance^2 of pt to infinite line
			let dist2 = dist ** 2;

			// calculated length^2 of line segment
			let calcrl2 = ln2 - dist2 + lnm12 - dist2;

			// redefine minimum distance to line segment (not infinite line) if necessary
			if (calcrl2 > rl2)
				dist = Math.sqrt(Math.min(ln2, lnm12));

			if ((minDist == null) || (minDist > dist))
			{
				if (calcrl2 > rl2)
				{
					if (lnm12 < ln2)
					{
						fTo = 0; //nearer to previous point
						fFrom = 1;
					}
					else
					{
						fFrom = 0; //nearer to current point
						fTo = 1;
					}
				}
				else
				{
					// perpendicular from point intersects line segment
					fTo = ((Math.sqrt(lnm12 - dist2)) / Math.sqrt(rl2));
					fFrom = ((Math.sqrt(ln2 - dist2)) / Math.sqrt(rl2));
				}
				minDist = dist;
				i = n;
			}
		}

		let dx = aXys[i - 1].x - aXys[i].x;
		let dy = aXys[i - 1].y - aXys[i].y;

		x = aXys[i - 1].x - (dx * fTo);
		y = aXys[i - 1].y - (dy * fTo);

	}

	return {
		'x': x,
		'y': y,
		'i': i,
		'fTo': fTo,
		'fFrom': fFrom
	};
}

function latLng2Point(latLng, map)
{
	let topRight = map.getProjection().fromLatLngToPoint(map.getBounds().getNorthEast());
	let bottomLeft = map.getProjection().fromLatLngToPoint(map.getBounds().getSouthWest());
	let scale = 2 ** map.getZoom();
	let worldPoint = map.getProjection().fromLatLngToPoint(latLng);
	return new google.maps.Point((worldPoint.x - bottomLeft.x) * scale, (worldPoint.y - topRight.y) * scale);
}

function point2LatLng(point, map)
{
	let topRight = map.getProjection().fromLatLngToPoint(map.getBounds().getNorthEast());
	let bottomLeft = map.getProjection().fromLatLngToPoint(map.getBounds().getSouthWest());
	let scale = 2 ** map.getZoom();
	let worldPoint = new google.maps.Point(point.x / scale + bottomLeft.x, point.y / scale + topRight.y);
	return map.getProjection().fromPointToLatLng(worldPoint);
}

function distancePointToPoint(p1, p2)
{
	const a = p1.x - p2.x;
	const b = p1.y - p2.y;
	return Math.sqrt(a * a + b * b);
}


function createElement(strElementName, arrElementProps, mxElementContent, arrElementEvents)
{
	arrElementProps = (typeof arrElementProps === "object") ? arrElementProps : {};
	arrElementEvents = (typeof arrElementEvents === "object") ? arrElementEvents : {};

	let elNew = document.createElement(strElementName);
	for (let i in arrElementProps)
	{
		elNew.setAttribute(i, arrElementProps[i]);
	}
	if (mxElementContent !== undefined)
	{
		if (isString(mxElementContent))
			elNew.appendChild(document.createTextNode(mxElementContent));
		else if (Array.isArray(mxElementContent))
			for (let i in mxElementContent)
			{
				elNew.appendChild(mxElementContent[i]);
			}
		else
			elNew.appendChild(mxElementContent);
	}

	if (arrElementEvents !== undefined)
	{
		for (let strEventName in arrElementEvents)
		{
			elNew.addEventListener(strEventName, arrElementEvents[strEventName]);
		}
	}

	return elNew;
}

function simplifyPath(path, tolerance)
{

	let points = path; // An array of google.maps.LatLng objects

	// Check there is something to simplify.
	if (points.length <= 2)
	{
		return points;
	}

	function distanceToSegment(p, v, w)
	{

		function distanceSquared(v, w)
		{
			return (v.x - w.x) ** 2 + (v.y - w.y) ** 2
		}

		function distanceToSegmentSquared(p, v, w)
		{

			let l2 = distanceSquared(v, w);
			if (l2 === 0) return distanceSquared(p, v);

			let t = ((p.x - v.x) * (w.x - v.x) + (p.y - v.y) * (w.y - v.y)) / l2;
			if (t < 0) return distanceSquared(p, v);
			if (t > 1) return distanceSquared(p, w);
			return distanceSquared(p, {x: v.x + t * (w.x - v.x), y: v.y + t * (w.y - v.y)});
		}

		// Lat/Lng to x/y
		function ll2xy(p)
		{
			if(p.lat instanceof Function)
				return {x: p.lat(), y: p.lng()};
			else
				return {x: p.lat, y: p.lng};
		}

		return Math.sqrt(distanceToSegmentSquared(ll2xy(p), ll2xy(v), ll2xy(w)));
	}

	//Ramer–Douglas–Peucker algorithm
	function dp(points, tolerance)
	{
		// If the segment is too small, just keep the first point.
		// We push the final point on at the very end.
		if (points.length <= 2)
		{
			return [points[0]];
		}

		let // An array of points to keep
			keep = []; // Index of said point

		let // Starting point that defines a segment
			v = points[0];

		let // Ending point that defines a segment
			w = points[points.length - 1];

		let // Distance of farthest point
			maxDistance = 0;

		let maxIndex = 0;

		// Loop over every intermediate point to find point greatest distance from segment
		for (let i = 1, ii = points.length - 2; i <= ii; i++)
		{
			let distance = distanceToSegment(points[i], points[0], points[points.length - 1]);
			if (distance > maxDistance)
			{
				maxDistance = distance;
				maxIndex = i;
			}
		}

		// check if the max distance is greater than our tollerance allows
		if (maxDistance >= tolerance)
		{

			// Recursivly call dp() on first half of points
			keep = keep.concat(dp(points.slice(0, maxIndex + 1), tolerance));

			// Then on second half
			keep = keep.concat(dp(points.slice(maxIndex, points.length), tolerance));

		}
		else
		{
			// Discarding intermediate point, keep the first
			keep = [points[0]];
		}
		return keep;
	}

	// Push the final point on
	const keep = dp(points, tolerance); // The simplified array of points
	keep.push(points[points.length - 1]);
	return keep;

}

function insertSorted(arr, item, comparator)
{
	//get the index we need to insert the item at
	let min = 0;
	let max = arr.length;
	let index = Math.floor((min + max) / 2);
	while (max > min)
	{
		if (comparator(item, arr[index]) < 0)
		{
			max = index;
		}
		else
		{
			min = index + 1;
		}
		index = Math.floor((min + max) / 2);
	}

	//insert the item
	arr.splice(index, 0, item);
}


function isString(arg)
{
	return typeof arg === 'string';
}

google.maps.Polyline.prototype.GetPointAtDistance = function(metres) {
	// some awkward special cases
	if (metres == 0) return this.getPath().getAt(0);
	if (metres < 0) return null;
	if (this.getPath().getLength() < 2) return null;
	var dist=0;
	var olddist=0;
	for (var i=1; (i < this.getPath().getLength() && dist < metres); i++) {
		olddist = dist;
		dist += this.getPath().getAt(i).distanceFrom(this.getPath().getAt(i-1));
	}
	if (dist < metres) {
		return null;
	}
	var p1= this.getPath().getAt(i-2);
	var p2= this.getPath().getAt(i-1);
	var m = (metres-olddist)/(dist-olddist);
	return {
		lat: p1.lat() + (p2.lat()-p1.lat())*m,
		lng: p1.lng() + (p2.lng()-p1.lng())*m
	};
};

google.maps.LatLng.prototype.distanceFrom = function(newLatLng) {
	return google.maps.geometry.spherical.computeDistanceBetween (this, newLatLng);
};


/*
 * object.watch polyfill
 *
 * 2012-04-03
 *
 * By Eli Grey, http://eligrey.com
 * Public Domain.
 * NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
 */

// object.watch
if (!Object.prototype.watch) {
	Object.defineProperty(Object.prototype, "watch", {
		enumerable: false
		, configurable: true
		, writable: false
		, value: function (prop, handler) {
			var
				oldval = this[prop]
				, newval = oldval
				, getter = function () {
					return newval;
				}
				, setter = function (val) {
					oldval = newval;
					return newval = handler.call(this, prop, oldval, val);
				}
			;

			if (delete this[prop]) { // can't watch constants
				Object.defineProperty(this, prop, {
					get: getter
					, set: setter
					, enumerable: true
					, configurable: true
				});
			}
		}
	});
}

// object.unwatch
if (!Object.prototype.unwatch) {
	Object.defineProperty(Object.prototype, "unwatch", {
		enumerable: false
		, configurable: true
		, writable: false
		, value: function (prop) {
			var val = this[prop];
			delete this[prop]; // remove accessors
			this[prop] = val;
		}
	});
}