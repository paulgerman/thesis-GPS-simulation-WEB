
function call(strMethodName, arrParams, fnSuccessCallback, fnErrorCallBack){
	$.jsonRPC.setup({
		endPoint: '../../thesisEndpoint'
	});

	console.log('Called ' + strMethodName + " with params ");
	console.log(JSON.stringify(arrParams));

	$.jsonRPC.request(strMethodName, {
		params: arrParams,
		success: (result) => {
			console.log('Result:');
			console.log(result);
			if(fnSuccessCallback !== undefined)
				fnSuccessCallback(result["result"]);
		},
		error: (result) => {
			console.error("Error " + result.error.code + ": " + result.error.message);
			if(fnErrorCallBack !== undefined)
				fnErrorCallBack(result.error);
		}
	});
}

function batchCall(arrRequests, fnSuccessCallback, fnErrorCallBack){
	$.jsonRPC.setup({
		endPoint: '../../thesisEndpoint'
	});
	console.log(arrRequests);
	$.jsonRPC.batchRequest(arrRequests, {
		success: (result) => {
			console.log('Batch called');
			console.log(arrRequests);
			console.log('Result:');
			console.log(result);
			if(fnSuccessCallback !== undefined)
				fnSuccessCallback(result["result"]);
		},
		error: (result) => {
			console.error("Error " + result.error.code + ": " + result.error.message);
			if(fnErrorCallBack !== undefined)
				fnErrorCallBack();
		}
	});
}