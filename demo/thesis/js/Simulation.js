class Simulation{
	constructor(/*Route*/ route, /*SimulationDataPoint[]*/ arrSimulationDataPoints, strName = "")
	{
		this.strName = strName;
		this.route = route;
		this.arrSimulationDataPoints = arrSimulationDataPoints;

		this.nTimeTotal = 0;

		//Next time step in the generation of the simulation (milliseconds)
		this.nTimeStep;

		//Current distance in the simulation (meters)
		this.nDistanceCurrent = 0;

		this.nSimulationDataPointIndexCurrent = 0;

		this.bSimulationEnd = false;
	}

	setMap(map)
	{
		this.map = map;
	}

	getCurrentLatLng()
	{
		return this.route.polyline.GetPointAtDistance(this.nDistanceCurrent);
	}

	getNextPoint()
	{
		this.updateCurrentSimulationDataPoint();

		const simulationDataPoint = this.arrSimulationDataPoints[this.nSimulationDataPointIndexCurrent];

		const nCurrentSpeed = this.getCurrentSpeed();
		//convert from km/h to m/s and calculate the traveled distance
		const distTraveled = (0.277777778 * nCurrentSpeed) / 1000 * this.nTimeStep;
		this.nTimeTotal += this.nTimeStep;

		//console.log("Traveled:" + distTraveled);
		this.nDistanceCurrent += distTraveled;

		if(this.nDistanceCurrent >= this.route.nTotalDistance)
		{
			this.nDistanceCurrent = this.route.nTotalDistance;
			this.bSimulationEnd = true;
		}

		const point = this.getCurrentLatLng();
		point.dist = this.nDistanceCurrent;
		point.alt_acc = simulationDataPoint.nAlt + this.rand(-1 * simulationDataPoint.nAltAcc, simulationDataPoint.nAltAcc, 0.01);

		point.bearing = this.computeBearing(this.pointLast, point);
		point.bearing_acc = 10 + this.rand(-2,2,0.1);

		point.speed = nCurrentSpeed;

		//reported speed accuracy
		point.speed_acc = simulationDataPoint.nSpeedAcc + this.rand(-1,1,2) * 0.05 * simulationDataPoint.nSpeedAcc;

		//reported location accuracy
		point.acc = simulationDataPoint.nAccuracy + this.rand(-1 * simulationDataPoint.nAccuracyAcc, simulationDataPoint.nAccuracyAcc, 0.01);

		//time to "wait" until generating the next point
		this.nTimeStep = (simulationDataPoint.nInterval + this.rand(-1,1,2) * simulationDataPoint.nIntervalAcc) * 1000;
		point.interval = this.nTimeStep;

		console.log(point);
		return point;
	}

	getCurrentSpeed()
	{
		const simulationDataPoint = this.arrSimulationDataPoints[this.nSimulationDataPointIndexCurrent];

		const nStartSpeed = simulationDataPoint.nSpeed;
		let nEndSpeed;
		if(this.arrSimulationDataPoints.length - 1 !== this.nSimulationDataPointIndexCurrent)
			nEndSpeed = this.arrSimulationDataPoints[this.nSimulationDataPointIndexCurrent + 1].nSpeed;
		else
			nEndSpeed = nStartSpeed;
		console.log("nStartSpeed | nEndSpeed: " + nStartSpeed + " | " + nEndSpeed);


		const nTraveledSegmentDist = this.nDistanceCurrent - simulationDataPoint.nDistFromStart;
		console.log("nTraveledSegmentDist: " + nTraveledSegmentDist);

		let nSegmentLength;
		if(this.arrSimulationDataPoints.length - 1 !== this.nSimulationDataPointIndexCurrent)
			nSegmentLength = this.arrSimulationDataPoints[this.nSimulationDataPointIndexCurrent + 1].nDistFromStart - simulationDataPoint.nDistFromStart;
		else
			nSegmentLength = this.route.nTotalDistance - simulationDataPoint.nDistFromStart;
		console.log("nSegmentLength: " + nSegmentLength);

		const nTraveledSegmentPercent = nTraveledSegmentDist / nSegmentLength;
		console.log("nTraveledSegmentPercent: " + nTraveledSegmentPercent);

		let nTransitionPercent;
		if(simulationDataPoint.bLinear)
			nTransitionPercent = nTraveledSegmentPercent;
		if(simulationDataPoint.bExponential)
			nTransitionPercent = (Math.pow(nTraveledSegmentPercent, 5) + 2*Math.pow(nTraveledSegmentPercent, 4)) / 3;
		if(simulationDataPoint.bAlgebraic)
			if(nTraveledSegmentPercent === 0)
				nTransitionPercent = 0;
			else
				nTransitionPercent = (3 * Math.pow(nTraveledSegmentPercent, 1/5) - 1) / 2;

		console.log("nTransitionPercent: " + nTransitionPercent);

		const nSpeed = (1 - nTransitionPercent) * nStartSpeed  + nTransitionPercent * nEndSpeed;
		console.log("Speed: " + nSpeed);
		return nSpeed;
	}

	updateCurrentSimulationDataPoint()
	{
		if(
			this.nSimulationDataPointIndexCurrent !== this.arrSimulationDataPoints.length - 1
			&& this.nDistanceCurrent >= this.arrSimulationDataPoints[this.nSimulationDataPointIndexCurrent + 1].nDistFromStart
		)
			this.nSimulationDataPointIndexCurrent++;
	}

	computeSimulationData()
	{
		const simulationDataPoint = this.arrSimulationDataPoints[this.nSimulationDataPointIndexCurrent];
		const arrData = [];
		const point = this.route.arrPath[0];
		point.acc = simulationDataPoint.nAccuracy;
		point.dist = this.nDistanceCurrent;
		point.alt_acc = simulationDataPoint.nAltAcc;
		point.bearing = 0;
		point.bearing_acc = 180;
		point.speed = simulationDataPoint.nSpeed;
		point.speed_acc = simulationDataPoint.nSpeedAcc;
		point.alt = this.route.arrElevation[0]["elevation"];
		this.nTimeStep = simulationDataPoint.nInterval;
		point.interval = this.nTimeStep;

		this.pointLast = point;
		arrData.push(point);

		while(!this.bSimulationEnd)
		{
			this.pointLast = this.getNextPoint();
			arrData.push(this.pointLast);
		}

		this.computeAltitude(arrData);
		console.log(arrData);
		console.log("Final length: " + arrData.length);

		return arrData;
	}

	save()
	{
		return new Promise((resolve, reject) => {
			const arrSimulationData = this.computeSimulationData();
			call("simulation_create", [{
				"user_id": arrUser["user_id"],
				"simulation_name": this.strName,
				"track_id": this.route.nID,
				"simulation_data": arrSimulationData,
				"simulation_total_time_s": this.nTimeTotal/1000,
				"simulation_access_code": ""
			}], (arrSimulation) =>
			{
				console.log(arrSimulation);
				resolve(arrSimulation);
			}, (error) => {
				reject(error);
			});
		});
	}


	computeAltitude(arrPath)
	{
		if (this.map === null || this.map === undefined)
		{
			console.error("Map undefined");
			return
		}

		const snapper = new cSnapToRoute();

		snapper.init(this.map, this.route.polyline);

		for(const i in arrPath)
		{
			const latLng = new google.maps.LatLng(arrPath[i]);
			const nDist = snapper.getDistAlongRoute(latLng);
			if(isNaN(nDist))
				arrPath[i].dist = 0;
			else
				arrPath[i].dist = nDist;
		}

		const arrElevationDistances = [];

		for(const i in this.route.arrElevation)
		{
			const latLng = new google.maps.LatLng(this.route.arrElevation[i]["location"]);

			const nDist = snapper.getDistAlongRoute(latLng);
			if(isNaN(nDist))
				arrElevationDistances[i] = 0;
			else
				arrElevationDistances[i] = nDist;
		}

		console.log(arrElevationDistances);
		console.log(this.route.arrElevation);

		let nCurrentElevationIndex = 0;
		for(const i in arrPath)
		{
			while(
				arrPath[i].dist > arrElevationDistances[nCurrentElevationIndex + 1]
				&& nCurrentElevationIndex !== this.route.arrElevation.length - 2
			)
				nCurrentElevationIndex++;

			const percentage =
				(arrPath[i].dist - arrElevationDistances[nCurrentElevationIndex])
				/(arrElevationDistances[nCurrentElevationIndex + 1] - arrElevationDistances[nCurrentElevationIndex]);

			console.log(nCurrentElevationIndex);

			const nAltitude =
				(1 - percentage) * this.route.arrElevation[nCurrentElevationIndex]["elevation"]
				+ percentage * this.route.arrElevation[nCurrentElevationIndex + 1]["elevation"];

			arrPath[i]["alt"] = nAltitude;
		}
		return arrPath;
	}

	rand(min, max, interval)
	{
		if (typeof(interval)==='undefined') interval = 1;
		const r = Math.floor(Math.random()*(max-min+interval)/interval);
		return r*interval+min;
	}

	computeBearing(p1, p2)
	{
		const φ1 = p1.lat;
		const φ2 = p2.lat;
		const λ1 = p1.lng;
		const λ2 = p2.lng;
		const y = Math.sin(λ2-λ1) * Math.cos(φ2);
		const x = Math.cos(φ1)*Math.sin(φ2) -
			Math.sin(φ1)*Math.cos(φ2)*Math.cos(λ2-λ1);
		const brng = Math.atan2(y, x) * (180/Math.PI);
		return brng;
	}
}