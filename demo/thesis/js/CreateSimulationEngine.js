class CreateSimulationEngine{
	constructor(arrTrack, elSimulationDataPanel, elSimulationDataPoint)
	{
		this.arrTrack = arrTrack;
		this.arrTrackData = arrTrack["track_data_vertices"];
		this.arrElevationData = arrTrack["track_data_altitude_probes"];

		this.map = new google.maps.Map(document.getElementById('map'), {
			clickableIcons: false
		});

		this.snapper = new cSnapToRoute();

		this.reset();
		this.generatePolyLine(this.arrTrackData);

		this.helpMarker = this.createMarker();
		this.helpMarker.setClickable(false);

		this.elSimulationDataPanel = elSimulationDataPanel;
		elSimulationDataPanel.appendChild(this.elSimulationData = createElement("div", {id: "simulationData"}));
		elSimulationDataPanel.appendChild(this.elSimulationControl = createElement("div", {id: "simulationControl"}));

		this.elInputSimulationName = createElement("input", {style: "margin-bottom: 5px;", class: "form-control input-sm", type:"text", placeholder: "Simulation name"});
        this.elInputSimulationName.addEventListener('keypress', e => {
			if (event.keyCode === 13) {
                this.saveFinalSimulation();
			}
		});

        this.elSimulationControl.appendChild(this.elInputSimulationName);

		this.elSimulationControl.appendChild(createElement("button", {class: 'btn btn-success'}, "Finalize simulation", {
			click: () => {
				this.saveFinalSimulation();
			}
		}));

		this.elSimulationDataPoint = elSimulationDataPoint;
		this.elSimulationDataPoint.style.display = "none";

		this.arrSimulationDataPoints = [];

		document.getElementById("save").addEventListener("click", (e) => {
			this.saveSimulationDataPoint(this.markerEditActive);
		});
		document.getElementById("delete").addEventListener("click", (e) => {
			this.deleteSimulationDataPoint(this.markerEditActive);
		});
		document.getElementById("close").addEventListener("click", (e) => {
			this.closeSimulationDataPoint();
		});

		document.getElementById("form-point").addEventListener("submit", (e) => {
            e.preventDefault();
            this.saveSimulationDataPoint(this.markerEditActive);
            return false;
		});

		this.markerEditActive = null;


		new google.maps.Marker({
			position: this.arrTrackData[0],
			icon: "https://maps.google.com/mapfiles/markerA.png",
			map: this.map,
			animation: google.maps.Animation.DROP,
			clickable: false
		});

		new google.maps.Marker({
			position: this.arrTrackData[this.arrTrackData.length - 1],
			icon: "https://maps.google.com/mapfiles/markerB.png",
			map: this.map,
			animation: google.maps.Animation.DROP,
			clickable: false
		});

		$.notify.defaults( {
			globalPosition: 'bottom center',
		} )
	}

	generatePolyLine(arrTrackData)
	{
		const polyLine = new google.maps.Polyline({
			path: arrTrackData,
			strokeColor: '#0000FF',
			strokeWeight: 7
		});

		const bounds = new google.maps.LatLngBounds();
		for (let i = 0; i < arrTrackData.length; i++)
			bounds.extend(arrTrackData[i]);
		this.map.fitBounds(bounds);
		polyLine.setMap(this.map);
		this.polyLine = polyLine;


		google.maps.event.addListenerOnce(this.map,"projection_changed", () => {
			this.snapper.init(this.map, this.polyLine);
			this.addPolyLineListeners();
			google.maps.event.addListener(this.polyLine, 'mousemove', this.updateHelper.bind(this));

			this.startSimulationDataPoint = this.createSimulationDataPoint(new google.maps.LatLng(arrTrackData[0]), 0, false);
			this.updateSimulationDataPanel();
		});

	}

	reset()
	{

	}

	addPolyLineListeners()
	{
		const deleteMenu = new DeleteMenu();

		google.maps.event.addListener(this.polyLine, 'rightclick', e =>
		{
			// Check if click was on a vertex control point
			if (e.vertex === undefined)
			{
				return;
			}
			deleteMenu.open(this.map, this.polyLine.getPath(), e.vertex);
		});

		this.iw = new google.maps.InfoWindow();


		google.maps.event.addListener(this.map, 'mousemove', this.updateHelper.bind(this));

		google.maps.event.addListener(this.map, 'click', (e) => {
			let snappedPosition = this.snapper.getClosestLatLng(e.latLng);

			const dist = distancePointToPoint(latLng2Point(e.latLng, this.map), latLng2Point(snappedPosition, this.map));
			if(dist < 100)
				this.clickEvent(e);
		});


		google.maps.event.addListener(this.polyLine, 'click', this.clickEvent.bind(this));
	}

	clickEvent(e)
	{
		let snappedPosition = this.snapper.getClosestLatLng(e.latLng);

		let nDistAlongRoute = this.snapper.getDistAlongRoute(snappedPosition);

		this.createSimulationDataPoint(snappedPosition, nDistAlongRoute);
	}

	createSimulationDataPoint(position, nDistAlongRoute, bShowIW = true)
	{
		if(bShowIW)
		{
			this.iw.setPosition(position);
			this.iw.setContent("you are at: " + position.toUrlValue(6) + "<br>" + "Distance (m) " + google.maps.geometry.spherical.computeLength(this.polyLine.getPath()).toFixed(2) + "<br>Distance along line (m): " + nDistAlongRoute.toFixed(2));
			console.log("sanity check, distance from start of Polyline: " + google.maps.geometry.spherical.computeDistanceBetween(position, this.polyLine.getPath().getAt(0)).toFixed(2));
			this.iw.open(this.map);
			setTimeout(() =>
			{
				this.iw.close();
			}, 1800);

			$.notify(
				"New point added to the simulation",
				"success"
			);
		}
		// console.log(evt.latLng.toUrlValue(6));

		const marker = this.createMarker(position, this.iw.getContent(), this.iw);

		const simulationDataPoint = new SimulationDataPoint(marker, nDistAlongRoute);
		this.setDefault(simulationDataPoint);


		//insert directly in the correct position
		insertSorted(this.arrSimulationDataPoints, simulationDataPoint, (a,b) => {
			return (a.nDistFromStart > b.nDistFromStart) ? 1 : ((b.nDistFromStart > a.nDistFromStart) ? -1 : 0);
		});

		const nPrevIndex = this.arrSimulationDataPoints.indexOf(simulationDataPoint) - 1;
		if(nPrevIndex !== -1)
		{
			for(let i in this.arrSimulationDataPoints[nPrevIndex])
                if (i !== "nDistFromStart" && i !== "marker")
                    simulationDataPoint[i] = this.arrSimulationDataPoints[nPrevIndex][i];

        }

		this.updateSimulationDataPanel();

		this.editSimulationDataPoint(marker);
		return simulationDataPoint;
	}

	updateHelper(e)
	{
		let snappedPosition = this.snapper.getClosestLatLng(e.latLng);
		const dist = distancePointToPoint(latLng2Point(e.latLng, this.map), latLng2Point(snappedPosition, this.map));
		if(dist < 100)
		{
			this.helpMarker.setPosition(snappedPosition);
			this.helpMarker.setVisible(true);
		}
		else
		{
			this.helpMarker.setVisible(false);
		}
	}

	saveFinalSimulation()
	{
		const route = new Route(this.arrTrackData, this.arrElevationData);
		route.setID(this.arrTrack["track_id"]);

		const simulation = new Simulation(route, this.arrSimulationDataPoints, this.elInputSimulationName.value);
		simulation.setMap(this.map);

		simulation.save().then((arrSimulation) => {
			$("#modal-title").text("Simulation " + arrSimulation["simulation_name"]);
			$('#QRCode').empty();
			$('#QRCode').qrcode(arrSimulation["simulation_access_code"]);
			$('#code').text(arrSimulation["simulation_access_code"]);
			$("#myModal").modal();
            $.notify(
                "Simulation saved!",
                "success"
            );
		});
	}


	createMarker(latLng, content, iw)
	{
		let marker = new google.maps.Marker({
			position: latLng,
			map: this.map,
			icon: {
				url: "https://maps.gstatic.com/intl/en_us/mapfiles/markers2/measle.png",
				size: new google.maps.Size(7, 7),
				anchor: new google.maps.Point(3.5, 3.5)
			}
		});

		if(content && iw)
		{
			google.maps.event.addListener(marker, 'click', (e) =>
			{
				this.editSimulationDataPoint(marker);
			});
			google.maps.event.addListener(marker, 'mouseover', function ()
			{
				iw.setContent(content);
				iw.open(this.map, marker);
			});

			google.maps.event.addListener(marker, 'mouseout', function ()
			{
				setTimeout(() =>
				{
					iw.close();
				}, 200);
			});
		}

		return marker;
	}

	updateSimulationDataPanel()
	{
		$(this.elSimulationData).empty();
		for(const i in this.arrSimulationDataPoints)
		{
			const elRow = createElement("div", {class: "dataPoint"}, "", {
				click: () => {
					this.editSimulationDataPoint(this.arrSimulationDataPoints[i].marker);
				},
				mouseenter: () => {
					if (this.arrSimulationDataPoints[i].marker.getAnimation() !== google.maps.Animation.BOUNCE) {
						this.arrSimulationDataPoints[i].marker.setAnimation(google.maps.Animation.BOUNCE);
					}
				},
				mouseleave: () => {
					if (this.arrSimulationDataPoints[i].marker.getAnimation() === google.maps.Animation.BOUNCE) {
						this.arrSimulationDataPoints[i].marker.setAnimation(null);
					}
				}
			});

			elRow.innerHTML = `
<i class="fa fa-tachometer fa-1" aria-hidden="true"></i>
${this.arrSimulationDataPoints[i].nSpeed} km/h
<div class="dataPointRight">
	${this.getStringForDistance(this.arrSimulationDataPoints[i].nDistFromStart)}
</div>`;
			this.elSimulationData.appendChild(elRow);
		}
	}

	editSimulationDataPoint(marker)
	{
		const simulationDataPoint = this.getSimulationDataPoint(marker);
		this.markerEditActive = marker;

		this.updatePanelInput(simulationDataPoint);
		this.elSimulationDataPoint.style.display = "block";
		this.elSimulationDataPanel.style.display = "none";
	}

	saveSimulationDataPoint(marker)
	{
		const simulationDataPoint = this.getSimulationDataPoint(marker);
		this.fetchPanelInput(simulationDataPoint);

		console.log(simulationDataPoint);

		this.updateSimulationDataPanel();
		this.closeSimulationDataPoint();

		$.notify(
			"Point data saved",
			"success"
		);
	}

	deleteSimulationDataPoint(marker)
	{
		if(this.startSimulationDataPoint.marker === marker)
		{
			$.notify(
				"You can't delete the first point :(",
				"error"
			);
			return;
		}
		const simulationDataPoint = this.getSimulationDataPoint(marker);

		const index = this.arrSimulationDataPoints.indexOf(simulationDataPoint);
		this.arrSimulationDataPoints.splice(index, 1);

		simulationDataPoint.marker.setMap(null);

		this.updateSimulationDataPanel();
		this.closeSimulationDataPoint();

		$.notify(
			"Point deleted from the simulation",
			"success"
		);
	}

	closeSimulationDataPoint()
	{
		this.elSimulationDataPoint.style.display = "none";
		this.elSimulationDataPanel.style.display = "block";
		this.markerEditActive = null;
	}

	getSimulationDataPoint(marker)
	{
		for(const i in this.arrSimulationDataPoints)
			if(this.arrSimulationDataPoints[i].marker === marker)
				return this.arrSimulationDataPoints[i];
		console.error("NOT FOUND");
		alert("not found :(");
	}

	fetchPanelInput(simulationDataPoint)
	{
		simulationDataPoint.nSpeed = Number(document.getElementById("speed").value);
		simulationDataPoint.nSpeedAcc = Number(document.getElementById("speed-acc").value);
		simulationDataPoint.bLinear = document.getElementById("lin").checked;
		simulationDataPoint.bExponential = document.getElementById("exp").checked;
		simulationDataPoint.bAlgebraic = document.getElementById("alg").checked;
		simulationDataPoint.nAccuracy = Number(document.getElementById("acc").value);
		simulationDataPoint.nAccuracyAcc = Number(document.getElementById("acc-acc").value);
		simulationDataPoint.nAlt = Number(document.getElementById("alt").value);
		simulationDataPoint.nAltAcc = Number(document.getElementById("alt-acc").value);
		simulationDataPoint.nInterval = Number(document.getElementById("interval").value);
		simulationDataPoint.nIntervalAcc = Number(document.getElementById("interval-acc").value);
	}

	updatePanelInput(simulationDataPoint)
	{
		console.log(simulationDataPoint);
		document.getElementById("speed").value = simulationDataPoint.nSpeed;
		document.getElementById("speed-acc").value = simulationDataPoint.nSpeedAcc;
		document.getElementById("lin").checked = simulationDataPoint.bLinear;
		document.getElementById("exp").checked = simulationDataPoint.bExponential;
		document.getElementById("alg").checked = simulationDataPoint.bAlgebraic;
		document.getElementById("acc").value = simulationDataPoint.nAccuracy;
		document.getElementById("acc-acc").value = simulationDataPoint.nAccuracyAcc;
		document.getElementById("alt").value = simulationDataPoint.nAlt;
		document.getElementById("alt-acc").value = simulationDataPoint.nAltAcc;
		document.getElementById("interval").value = simulationDataPoint.nInterval;
		document.getElementById("interval-acc").value = simulationDataPoint.nIntervalAcc;
	}

	getStringForDistance(nDist)
	{
		if(nDist > 1000)
			return (nDist/1000).toFixed(2) + " km";
		else
			return Math.round(nDist) + " m";
	}

	setDefault(simulationDataPoint)
	{
        simulationDataPoint.nSpeed = 100;
        simulationDataPoint.nSpeedAcc = 0;
        simulationDataPoint.bLinear = true;
        simulationDataPoint.bExponential = false;
        simulationDataPoint.bAlgebraic = false;
        simulationDataPoint.nAccuracy = 20;
        simulationDataPoint.nAccuracyAcc = 2;
        simulationDataPoint.nAlt = 10;
        simulationDataPoint.nAltAcc = 1;
        simulationDataPoint.nInterval = 5;
		simulationDataPoint.nIntervalAcc = 1;
        return simulationDataPoint;
	}
}