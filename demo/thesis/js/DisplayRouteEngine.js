class DisplayRouteEngine{
	constructor(strID, arrTrackData)
	{
		this.map = new google.maps.Map(document.getElementById(strID), {
			draggable: false,
			zoomControl: false,
			scrollwheel: false,
			disableDoubleClickZoom: true,
			disableDefaultUI: true
		});

		this.arrTrackData = arrTrackData;
		this.generatePolyLine(arrTrackData);
	}

	generatePolyLine(arrTrackData)
	{
		console.log(arrTrackData);
		const polyLine = new google.maps.Polyline({
			path: arrTrackData,
			strokeColor: '#0000FF',
			strokeWeight: 3
		});

		const bounds = new google.maps.LatLngBounds();
		for (let i = 0; i < arrTrackData.length; i++)
			bounds.extend(arrTrackData[i]);
		this.map.fitBounds(bounds);
		polyLine.setMap(this.map);

		google.maps.event.addDomListener(window, "resize", () => {
			this.map.fitBounds(bounds);
		});
	}
}