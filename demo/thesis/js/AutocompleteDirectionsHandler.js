
/**
 * @constructor
 */
class AutocompleteDirectionsHandler {
	constructor(map, directionsService, directionsDisplay) {
		this.bMapSelectMode = false;

		this.map = map;
		this.originPlaceId = null;
		this.destinationPlaceId = null;

		this.originLatLng = null;
		this.destinationLatLng = null;

		this.travelMode = 'WALKING';
		this.originInput = document.getElementById('origin-input');
		this.destinationInput = document.getElementById('destination-input');
		this.modeSelector = document.getElementById('mode-selector');
		this.directionsService = directionsService;
		this.directionsDisplay = directionsDisplay;
		this.directionsDisplay.setMap(map);

		const originAutocomplete = new google.maps.places.Autocomplete(
			this.originInput, {placeIdOnly: true});
		const destinationAutocomplete = new google.maps.places.Autocomplete(
			this.destinationInput, {placeIdOnly: true});

		this.setupClickListener('changemode-walking', 'WALKING');
		this.setupClickListener('changemode-transit', 'TRANSIT');
		this.setupClickListener('changemode-driving', 'DRIVING');

		this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
		this.setupPlaceChangedListener(destinationAutocomplete, 'DEST');

		this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(this.originInput);
		this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(this.destinationInput);
		this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(this.modeSelector);



		const fnFocusIn = (e) => {
			console.log(e.target);
			const eOrigin = e;
			this.eventListener = google.maps.event.addListener(this.map, 'click', (e) => {
				if(eOrigin.target.id === this.originInput.id)
				{
					this.originInput.value = e.latLng;
					this.originLatLng = e.latLng;
					this.originPlaceId = null;
					originAutocomplete.set('place',null);
				}
				else if(eOrigin.target.id === this.destinationInput.id)
				{

					this.destinationInput.value = e.latLng;
					this.destinationLatLng = e.latLng;
					this.destinationPlaceId = null;
					destinationAutocomplete.set('place',null);
				}
				else
					console.error("WTF");
				//this.placeChanged();
				google.maps.event.removeListener(this.eventListener);
			});
		};


		this.originInput.addEventListener("focusin", fnFocusIn);
		this.destinationInput.addEventListener("focusin", fnFocusIn);
	}

	// Sets a listener on a radio button to change the filter type on Places
	// Autocomplete.
	setupClickListener(id, mode) {
		const radioButton = document.getElementById(id);
		const me = this;
		radioButton.addEventListener('click', () => {
			me.travelMode = mode;
			me.route();
		});
	}

	setupPlaceChangedListener(autocomplete, mode) {
		const me = this;
		autocomplete.bindTo('bounds', this.map);

		this.placeChanged = () => {
			google.maps.event.removeListener(this.eventListener);
			console.log("place changed");
			const place = autocomplete.getPlace();

			if (mode === 'ORIG')
			{
				if(place && place.place_id)
				{
					me.originPlaceId = place.place_id;
					me.originLatLng = null;
				}
			}
			else
			{
				if(place && place.place_id)
				{
					me.destinationPlaceId = place.place_id;
					me.destinationLatLng = null;
				}
			}
			me.route();
		};
		autocomplete.addListener('place_changed', this.placeChanged);

	}

	route() {
		if (
			(!this.originPlaceId && !this.originLatLng)
			|| (!this.destinationPlaceId && !this.destinationLatLng)
		){
			return;
		}
		const me = this;

		let origin, destination;
		if(this.originPlaceId)
			origin = {'placeId': this.originPlaceId};
		else
			origin = this.originLatLng;

		if(this.destinationPlaceId)
			destination = {'placeId': this.destinationPlaceId};
		else
			destination = this.destinationLatLng;

		console.log(origin);
		console.log(destination);

		this.directionsService.route({
			origin: origin,
			destination: destination,
			travelMode: this.travelMode
		}, (response, status) => {
			if (status === 'OK') {
				me.directionsDisplay.setDirections(response);
			} else {
				window.alert(`Directions request failed due to ${status}`);
			}
		});
	}

	reset() {
		this.directionsDisplay.setMap(this.map);
		this.directionsDisplay.set('directions', null);

		this.originInput.value = "";
		this.destinationInput.value = "";
		document.getElementById('changemode-walking').selected = true;

		this.originPlaceId = null;
		this.destinationPlaceId = null;

		this.originLatLng = null;
		this.destinationLatLng = null;
		this.enableInput();
	}

	disableInput()
	{
		this.originInput.disabled = true;
		this.destinationInput.disabled = true;
		$("#mode-selector :input").attr("disabled", true);

	}

	enableInput()
	{
		this.originInput.disabled = false;
		this.destinationInput.disabled = false;
		this.modeSelector.disabled = false;
		$("#mode-selector :input").attr("disabled", false);
	}
}