class Route
{
	constructor(arrPath, arrElevation, strName = "")
	{
		this.polyline = new google.maps.Polyline({
			path: arrPath,
		});
		this.arrPath = arrPath;
		this.arrElevation = arrElevation;
		this.strName = strName;
		this.nTotalDistance = this.getTotalDistance();
	}

	getTotalDistance()
	{
		let total = 0;
		const path = this.polyline.getPath();
		for (let i = 0; i < this.arrPath.length - 1; i++) {
			total += google.maps.geometry.spherical.computeDistanceBetween(path.getAt(i), path.getAt(i+1));
		}
		return total;
	}

	setID(nID)
	{
		this.nID = nID;
	}

	makePreviewURL()
	{
		//simplify path to allow elevation data to be retrieved (due to URL size limit when requesting the data)
		let tmpPath = JSON.parse(JSON.stringify(this.arrPath));

		let encoded = Route.makeURL(tmpPath);
		let tol = 0.0001;
		let i = 1;
		while (encoded.length > 5000)
		{
			const nPathLength = tmpPath.length;
			const nEncodedLength = encoded.length;
			tmpPath = simplifyPath(tmpPath, tol);
			encoded = Route.makeURL(tmpPath);
			tol += .002;
			console.log("Simplified path #" + i + " From " + nPathLength + " to "+ tmpPath.length + " | " + nEncodedLength + " to " + encoded.length);
			i++;
		}

		return "https://maps.googleapis.com/maps/api/staticmap?size=640x200&key=AIzaSyAnsHmFgBGEO8E5DYh5P3kHUZyFtu-hLcs&path=" + encoded;
	}

	static makeURL(path)
	{
		let url = "";
		for(const i in path)
			url += path[i].lat + "," + path[i].lng + "|";
		return url.slice(0,-1);
	}

	save()
	{
		call("track_create", [{
			"user_id": arrUser["user_id"],
			"track_data_vertices": this.arrPath,
			"track_name": this.strName,
			"track_length_m": this.getTotalDistance(),
			"track_data_altitude_probes": this.arrElevation,
			"track_preview_url": this.makePreviewURL()
		}], (arrTrack) => {
			console.log(arrTrack);
			$.notify(
				"Route saved!",
				"success"
			);
		});
	}
}