class CreateRouteEngine{
	constructor()
	{
		this.applicationState = BUILD_ROUTE;
		this.map = new google.maps.Map(document.getElementById('map'), {
			zoom: 13,
			center: {
				lat: 40.771,
				lng: -73.974
			}
		});

		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition((position) => {
				const initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				this.map.setCenter(initialLocation);
			});
		}

		let elControlPanel = document.getElementById('floating-panel');
		this.map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(elControlPanel);

		this.polyLinesArray = [];

		this.directionsService = new google.maps.DirectionsService;

		this.directionsDisplay = new google.maps.DirectionsRenderer({
			map: this.map,
			draggable: true
		});

		this.directionsDisplay.addListener('directions_changed', () =>
		{
			this.updateTrackLengthGUI();
		});
		this.elButtonCancelEdit = document.getElementById('cancelEditing');
		this.elButtonCancelEdit.addEventListener('click', () => {
			for(let i in this.polyLinesArray)
				this.polyLinesArray[i].setEditable(false);
		});

		this.elButtonSave = document.getElementById('saveRoute');
		this.elButtonSave.addEventListener("click", this.saveFinalPath.bind(this));

		this.elInputName = document.getElementById('track_name');


		this.elButtonNext = document.getElementById('next');
		this.elButtonNext.addEventListener("click", this.nextApplicationState.bind(this));
		this.autocompleteDirectionsHandler = new AutocompleteDirectionsHandler(this.map, this.directionsService, this.directionsDisplay);
		this.reset();

		$.notify.defaults( {
			globalPosition: 'bottom center',
		} )
	}

	nextApplicationState()
	{
		if(this.applicationState === BUILD_ROUTE)
		{
			this.directionsDisplay.setMap(null);
			this.generatePolyLine();
			this.elButtonNext.innerText = "Reset route";
			this.applicationState = EDIT_ROUTE;
			this.autocompleteDirectionsHandler.disableInput();
		}
		else if(this.applicationState === EDIT_ROUTE)
		{
			this.reset();
		}
	}

	computeTotalDistanceDirections()
	{
		let total = 0;
		const directions = this.directionsDisplay.getDirections();
		if(directions !== null && directions.routes.length !== 0)
		{
			const myroute = directions.routes[0];
			for (let i = 0; i < myroute.legs.length; i++)
			{
				total += myroute.legs[i].distance.value;
			}
		}
		else
			total = 0;

		return total;
	}

	computeTotalDistancePolyLine()
	{
		let total = 0;
		for(let i in this.polyLinesArray)
		{
			total += google.maps.geometry.spherical.computeLength(this.polyLinesArray[i].getPath());
		}
		return total;
	}

	getTrackLength()
	{
		if(this.applicationState === BUILD_ROUTE)
			return this.computeTotalDistanceDirections();
		else
			return this.computeTotalDistancePolyLine();
	}

	updateTrackLengthGUI()
	{
		let total = this.getTrackLength();
		total = round(total / 1000, 3);
		document.getElementById('total').innerHTML = `${total} km`;
	}

	removePolyLines()
	{
		for (let i = 0; i < this.polyLinesArray.length; i++)
		{
			this.polyLinesArray[i].setMap(null);
		}
		this.polyLinesArray.length = 0;
	}

	generatePolyLine()
	{
		let fnCreatePolyLine = () => {
			const polyLine = new google.maps.Polyline({
				path: [],
				strokeColor: '#0000FF',
				strokeWeight: 3,
				//editable: true,
			});
			return polyLine;
		};

		let polyLine = fnCreatePolyLine();
		this.polyLinesArray.push(polyLine);

		if(this.directionsDisplay.getDirections() === null)
		{
			const path = polyLine.getPath();
			const center = this.map.getCenter();

			const scale = this.getScale();
			console.log(scale);
			const nFactor = 0.6 * 0.003;
			path.push(new google.maps.LatLng({lat: center.lat() + scale[1] * nFactor, lng: center.lng() + scale[0] * nFactor}));
			path.push(new google.maps.LatLng({lat: center.lat() - scale[1] * nFactor, lng: center.lng() - scale[0] * nFactor}));
        }
        else
		{
			const legs = this.directionsDisplay.getDirections().routes[0].legs;
			for (let i = 0; i < legs.length; i++) {
				const steps = legs[i].steps;
				for (let j = 0; j < steps.length; j++) {
					const nextSegment = steps[j].path;
					for (let k = 0; k < nextSegment.length; k++) {
						const path = polyLine.getPath();
						path.push(nextSegment[k]);

						if (path.getLength() === maxPointsInPolyLine) {
							polyLine = fnCreatePolyLine();
							this.polyLinesArray.push(polyLine);
							const path = polyLine.getPath();
							path.push(nextSegment[k]);
						}
					}
				}
			}
		}


		for(let i in this.polyLinesArray)
			this.addPolyLineListeners(this.polyLinesArray[i]);

		for(let i in this.polyLinesArray)
			this.polyLinesArray[i].setMap(this.map);
		this.updateTrackLengthGUI();
	}
	
	getScale()
	{
		let bounds = this.map.getBounds();

		// from that, get the coordinates for the NE and SW corners
		let NE = bounds.getNorthEast();
		let SW = bounds.getSouthWest();

		// from that, figure out the latitudes and the longitudes
		let lat1 =  NE.lat();
		let lat2 =  SW.lat();

		let lng1 =  NE.lng();
		let lng2 =  SW.lng();

		// construct new LatLngs using the coordinates for the horizontal distance between lng1 and lng2
		let horizontalLatLng1 = new google.maps.LatLng(lat1,lng1);
		let horizontalLatLng2 = new google.maps.LatLng(lat1,lng2);

		// construct new LatLngs using the coordinates for the vertical distance between lat1 and lat2
		let verticalLatLng1 = new google.maps.LatLng(lat1,lng1);
		let verticalLatLng2 = new google.maps.LatLng(lat2,lng1);

		// work out the distance horizontally
		let horizontal = google.maps.geometry.spherical.computeDistanceBetween(horizontalLatLng1,horizontalLatLng2);

		// work out the distance vertically
		let vertical = google.maps.geometry.spherical.computeDistanceBetween(verticalLatLng1,verticalLatLng2);

		// round to kilometres to 1dp
		let horizontalkm = this.convertMetresToKm(horizontal);
		let verticalkm = this.convertMetresToKm(vertical);

		return [horizontalkm, verticalkm];
	}

	convertMetresToKm(metres) {
		return Math.round(metres / 1000 *10)/10;    // distance in km rounded to 1dp
	}

	reset()
	{
		this.removePolyLines();
		this.elButtonNext.innerText = "Start fine tuning";
		this.applicationState = BUILD_ROUTE;
		this.elButtonCancelEdit.style.display = "none";
		this.autocompleteDirectionsHandler.reset();
	}

	addPolyLineListeners(polyLine)
	{
		const deleteMenu = new DeleteMenu();

		google.maps.event.addListener(polyLine, 'rightclick', e =>
		{
			// Check if click was on a vertex control point
			if (e.vertex === undefined)
			{
				return;
			}
			deleteMenu.open(this.map, polyLine.getPath(), e.vertex);
		});
		google.maps.event.addListener(polyLine, 'click', e =>
		{
			if(!polyLine.getEditable())
			{
				polyLine.setEditable(true);
				this.elButtonCancelEdit.style.display = "initial";
			}
		});

		const fnKeepPolyLinesConnected = e =>{
			const pathClicked = polyLine.getPath();

			//reconnect in case it's the last vertex - or if it's the last vertex that has been removed
			if(e === pathClicked.getLength() - 1 || e === pathClicked.getLength())
			{
				const index = this.polyLinesArray.indexOf(polyLine);
				if(this.polyLinesArray.indexOf(polyLine) !== this.polyLinesArray.length - 1)
				{
					const path = this.polyLinesArray[index+1].getPath();
					if(path.getAt(0) !== pathClicked.getAt(pathClicked.getLength() - 1))
						path.setAt(0, pathClicked.getAt(pathClicked.getLength() - 1));
				}
			}
			//reconnect in case it's the first vertex
			if(e === 0)
			{
				const index = this.polyLinesArray.indexOf(polyLine);
				if(this.polyLinesArray.indexOf(polyLine) !== 0)
				{
					const path = this.polyLinesArray[index-1].getPath();
					if(path.getAt(path.getLength()-1) !== pathClicked.getAt(0))
						path.setAt(path.getLength()-1, pathClicked.getAt(0));
				}
			}

		};

		google.maps.event.addListener(polyLine.getPath(), "set_at", e => {
			fnKeepPolyLinesConnected(e);
			setTimeout(this.updateTrackLengthGUI(), 0);

		});
		google.maps.event.addListener(polyLine.getPath(), "remove_at", e => {
			fnKeepPolyLinesConnected(e);
			setTimeout(this.updateTrackLengthGUI(), 0);
		});
		google.maps.event.addListener(polyLine, "dragend", e => {
			setTimeout(this.updateTrackLengthGUI(), 0);
		});
		google.maps.event.addListener(polyLine.getPath(), "insert_at", e => {
			setTimeout(this.updateTrackLengthGUI(), 0);
		});
	}

	getFinalPath()
	{
		const pointsArray = [];
		for(let i in this.polyLinesArray)
		{
			pointsArray.push(...this.polyLinesArray[i].getPath().getArray());
		}
		return pointsArray;
	}

	getPathElevation()
	{
		return new Promise((resolve, reject) =>
		{
			let path = [];
			for (let i in this.polyLinesArray)
			{
				let tmpPath = this.polyLinesArray[i].getPath();
				tmpPath.forEach((item) =>
				{
					path.push(item);
				});
			}

			//simplify path to allow elevation data to be retrieved (due to URL size limit when requesting the data)
			let encoded = google.maps.geometry.encoding.encodePath(path);
			let tol = 0.0001;
			let i = 1;
			while (encoded.length > 5000)
			{
				const nPathLength = path.length;
				const nEncodedLength = encoded.length;
				path = simplifyPath(path, tol);
				encoded = google.maps.geometry.encoding.encodePath(path);
				tol += .002;
				console.log("Simplified path #" + i + " From " + nPathLength + " to "+ path.length + " | " + nEncodedLength + " to " + encoded.length);
				i++;
			}


			let elevator = new google.maps.ElevationService;
			elevator.getElevationAlongPath({
				'path': path,
				'samples': 512
			}, (elevations, status) =>
			{
				if (status !== 'OK')
				{
					// Show the error code inside the chartDiv.
					alert('Cannot show elevation: request failed because ' + status);
					reject(status);
				}
				resolve(elevations);
			});
		});
	}

	async saveFinalPath()
	{
		if(this.applicationState === BUILD_ROUTE)
			this.nextApplicationState();

		const route = new Route(this.getFinalPath(), await this.getPathElevation(), this.elInputName.value);
		route.save();
	}
}

const maxPointsInPolyLine = 50;
const BUILD_ROUTE = 0;
const EDIT_ROUTE = 1;