class SimulationDataPoint{
	constructor(marker, nDistFromStart, nSpeed = 0)
	{
		this.marker = marker;
		this.nDistFromStart = nDistFromStart;

		//Speed meters/second
		this.nSpeed = nSpeed;
	}
}