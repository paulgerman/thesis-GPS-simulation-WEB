<?php
function renderPageGeneral($arrUser)
{
	$clientController = new \Controllers\ThesisUserController();
	$arrClients = $clientController->get_all_internal(0, 4, "");

	$trackController = new \Controllers\ThesisTrackController();
	$simulationController = new \Controllers\ThesisSimulationController();

	$nTracks = $trackController->count_internal(" WHERE `user_id` = ".(int)$arrUser["user_id"]);
	$nSimulations = $simulationController->count_internal(" WHERE `user_id` = ".(int)$arrUser["user_id"]);

	$arrSimulations = $simulationController->get_all_internal(0, 4, " WHERE `user_id` = ".(int)$arrUser["user_id"], "simulation_created_time", "DESC");

	$sessionController = new \Controllers\ThesisUserSessionController();
	$arrSessions = $sessionController->get_all_internal(0, 5, " WHERE `user_id` = ".trdb()->quote($arrUser["user_id"]), "user_session_start_date", "DESC");
	?>

	<div style="padding: 30px 20px 20px 20px; width: 100%">
		<div class="col-sm-4" style="width: 400px;">
			<div class="col-sm-6 clients-header active">
				<div class="clients-number"><?=$nSimulations?></div>
				<div class="clients-text">SIMULATIONS</div>
			</div>
			<div class="col-sm-6 clients-header">
				<div class="clients-number"><?=$nTracks?></div>
				<div class="clients-text">TRACKS</div>
			</div>
			<div class="col-sm-12 clients-rows equal" style="padding: 0;">
				<?php
				foreach($arrSimulations as $arrSimulation)
				{
					echo '
				<div class="col-sm-6 clients-name">
					'.$arrSimulation["simulation_name"].'
				</div>
				<div class="col-sm-6">
					'.$arrSimulation["simulation_total_time_s"].' seconds
				</div>';
				}
				?>
				<div class="col-sm-12 text-right">
					<a href="?page=simulations">Show all</a>
				</div>
			</div>
		</div>

		<div class="col-sm-8" style="width:calc(100% - 400px); margin-top: 90px;">
			<div class="visits">
				<div class="col-sm-12 visits-header">
					LAST LOGINS
				</div>
				<?php
				foreach($arrSessions as $arrSession)
				{
					echo '
				<div class="col-sm-12 visits-row">
					<img class="img-circle" src="'.($arrUser["user_pic"] == ""?"img/default_avatar.png":$arrUser["user_pic"]).'">
					<span class="visits-agent">'.($arrSession["user_session_ip"]=="::1"?"localhost":$arrSession["user_session_ip"]).'</strong>
					<span class="pull-right">'.$arrSession["user_session_start_date"].'</span>
				</div>';
				}
				?>

				<div class="col-sm-12 text-right">

				</div>
			</div>
		</div>
	</div>


<?php
}