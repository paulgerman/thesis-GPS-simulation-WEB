<?php
function renderPageSimulations($arrUser)
{
	?>
	<div class="panel">
		<div class="panel-top">
			<div class="panel-title">
				AVAILABLE SIMULATIONS
			</div>
			<div class="panel-filter">
			</div>
		</div>
		<hr>
		<div class="panel-content" style="padding-left: 30px; padding-right: 30px;">
			<?php displaySimulations($arrUser); ?>
		</div>
	</div>
	<?php
}

function displaySimulations($arrUser)
{
	$controllerTrack = new \Controllers\ThesisTrackController();

	$controllerSimulation = new \Controllers\ThesisSimulationController();
	if(isset($_GET["delete"]))
	    try {
		    $controllerSimulation->delete($_GET["delete"]);
	    }
	    catch(GeneralException $exc)
        {
            if($exc->getCode() !== GeneralException::PRODUCT_NOT_FOUND)
                throw $exc;
        }

	$arrSimulations = $controllerSimulation->get_all_internal(0, 99999, " WHERE `user_id` = ".trdb()->quote($arrUser["user_id"]));


	$arrTracksSimulations = [];
	foreach($arrSimulations as $arrSimulation)
	{
		if(array_key_exists($arrSimulation["track_id"], $arrTracksSimulations))
			$arrTracksSimulations[$arrSimulation["track_id"]][] = $arrSimulation;
		else
			$arrTracksSimulations[$arrSimulation["track_id"]] = array($arrSimulation);
	}

	?>
	<script>
		function displaySimulation(strName, strCode)
		{
			$("#modal-title").text("Simulation " + strName);
			$('#QRCode').empty();
			$('#QRCode').qrcode(strCode);
			$('#code').text(strCode);
			$("#myModal").modal();
		}
	</script>
	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 id="modal-title" class="modal-title"></h4>
				</div>
				<div class="modal-body" style="text-align: center;">
					<div id="QRCode" style="margin-bottom: 15px;"></div>
					<span style="font-size: 20px; font-weight: bold" id="code"></span>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<?php
        if(count($arrTracksSimulations) === 0)
        {
            echo "You didn't create any simulations yet!";
        }
		foreach($arrTracksSimulations as $nTrackID => $arrTrackSimulations)
		{
			$arrTrack = $controllerTrack->get($nTrackID);
			?>

			<div class="col-sm-6 col-md-4 track-preview">
				<div class="track-name">
					Track: <?=$arrTrack["track_name"]?>
				</div>
				<div id="map<?=$arrTrack["track_id"]?>" class="map-preview">
				</div>
				<div class="simulations">
					Simulations: <br>
					<?php
                    if(count($arrTrackSimulations) === 0)
                    {
                        echo "This track doesn't have any simulation!";
                    }
					foreach($arrTrackSimulations as $arrSimulation)
					{
						echo '
						<a href="#" onclick="displaySimulation(\''.$arrSimulation["simulation_name"].'\',\''.$arrSimulation["simulation_access_code"].'\');">
							<b>'.$arrSimulation["simulation_name"].'</b>
						</a>
						<a href="?page=simulations&delete='.$arrSimulation["simulation_id"].'">[x]</a>
						<br>';
					}
					?>
				</div>

				<script>
					new DisplayRouteEngine('map<?=$arrTrack["track_id"]?>', <?=json_encode($arrTrack["track_data_vertices"])?>);
				</script>
			</div>

			<?php
		}
		?>
	</div>

	<?php
}