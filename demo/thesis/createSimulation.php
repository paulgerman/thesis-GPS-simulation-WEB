<?php
function renderPageCreateSimulation($arrUser)
{
	if(isset($_GET["track"]))
	{
		$controllerTrack = new \Controllers\ThesisTrackController();
		$arrTrack = $controllerTrack->get($_GET["track"]);
		?>

		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 id="modal-title" class="modal-title"></h4>
					</div>
					<div class="modal-body" style="text-align: center;">
						<div id="QRCode" style="margin-bottom: 15px;"></div>
						<span style="font-size: 20px; font-weight: bold" id="code"></span>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

		<div class="">
			<div class="col-xs-10" style="padding: 0; width: calc(100% - 300px)">
				<div id="map"></div>
			</div>
			<div id="simulationDataPanel" class="col-xs-2" style="width: 300px;">

			</div>
			<div id="simulationDataPoint" class="col-xs-2" style="width: 300px; text-align: center;">
				<form id="form-point">
					<div class="form-group">
						<label for="speed">Speed:</label><br>
                        <div class="form-input">
						    <input type="number" class="form-control" id="speed" min="1">
                            <div class="form-input-text" >km/h ±</div>
						    <input type="number" class="form-control" id="speed-acc" min="0">
                        </div>
					</div>
                    <div class="form-group">
						<label>Speed transition:</label><br>
                        <div class="form-input">
                            <div class="opt-group">
                                <label for="lin"><img src="img/lin.png"></label><br>
                                <input type="radio" name="transition" value="lin" id="lin">
                                <label for="lin">Linear</label>
                            </div>
                            <div class="opt-group">
                                <label for="exp"><img src="img/exp.png"></label><br>
                                <input type="radio" name="transition" value="exp" id="exp">
                                <label for="exp">Polynomial</label>
                            </div>
                            <div class="opt-group">
                                <label for="alg"><img src="img/alg.png"></label><br>
                                <input type="radio" name="transition" value="alg" id="alg">
                                <label for="alg">Algebraic</label>
                            </div>
                        </div>
					</div>
                    <div class="form-group">
                        <label for="acc">Location accuracy:</label><br>
                        <div class="form-input">
						    <input type="number" class="form-control" id="acc" min="0">
                            <div class="form-input-text">m ±</div>
                            <input type="number" class="form-control" id="acc-acc" min="0">
                        </div>
					</div>
                    <div class="form-group">
                        <label for="alt">Altitude accuracy:</label><br>
                        <div class="form-input">
						    <input type="number" class="form-control" id="alt" min="0">
                            <div class="form-input-text">m ±</div>
                            <input type="number" class="form-control" id="alt-acc" min="0">
                        </div>
					</div>
                    <div class="form-group">
                        <label for="interval">Location update interval:</label><br>
                        <div class="form-input">
						    <input type="number" class="form-control" id="interval" min="1">
                            <div class="form-input-text">s ±</div>
                            <input type="number" class="form-control" id="interval-acc" min="0">
                        </div>
					</div>
					<button type="button" class="btn btn-success" id="save">Save</button>
					<button type="button" class="btn btn-danger" id="delete">Delete</button>
					<button type="button" class="btn btn-default" id="close">Close</button>

				</form>
			</div>
		</div>

		<script>
			new CreateSimulationEngine(<?=json_encode($arrTrack)?>, document.getElementById('simulationDataPanel'), document.getElementById('simulationDataPoint'));
		</script>

		<?php
	}
	else
	{
		?>
		<div class="panel">
			<div class="panel-top">
				<div class="panel-title">
					Select track to build simulation on
				</div>
				<div class="panel-filter">
				</div>
			</div>
			<hr>
			<div class="panel-content" style="padding-left: 30px; padding-right: 30px;">
				<?php
				require_once ("tracks.php");
				displayTracks($arrUser,"?page=createSimulation&track=");
				?>
			</div>
		</div>
		<?php
	}
	?>

	<?php
}