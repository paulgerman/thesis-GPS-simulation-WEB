<?php
class Session
{
	public $arrSession;
	public $arrUser;

	function __construct()
	{
		$this->controllerUserSession = new \Controllers\ThesisUserSessionController();
		$this->controllerUser = new \Controllers\ThesisUserController();
		$this->arrSession = array();
	}

	function create($strUserEmail, $strPassword, $bRemember)
	{
		if($bRemember)
			$nValidSeconds = 3600*24*7;
		else
			$nValidSeconds = 3600;

		$arrUser = $this->controllerUser->get($strUserEmail, "user_email");
		$strPassword = md5($strPassword.$arrUser["user_salt"]);
		if($strPassword === $arrUser["user_password"])
		{
			$this->arrSession = $this->controllerUserSession->create(array(
				"user_id" => $arrUser["user_id"],
				"user_session_end_date" => date('Y-m-d H:i:s', time() + $nValidSeconds),
				"user_session_start_date" => date('Y-m-d H:i:s', time()),
				"user_session_hash" => $arrUser["user_id"].'|'.md5(uniqid(rand(), true)),
				"user_session_ip" => $_SERVER["REMOTE_ADDR"]
			));
			$this->arrUser = $arrUser;
		}
		else
		{
			throw new \GeneralException("Invalid password", GeneralException::INVALID_PASSWORD);
		}
	}

	function storeCookie()
	{
		setcookie("session_man", $this->arrSession["user_session_hash"], time()+60*60*24*30);
	}

	function deleteCookie()
	{
		unset($_COOKIE["session_man"]);
		setcookie("session_man", '', time()-3600);
	}

	function delete()
	{
		$this->arrSession["user_session_terminated"] = 1;
		$this->arrSession = $this->controllerUserSession->edit($this->arrSession["user_session_id"], $this->arrSession);
	}

	function load($strSessionHash)
	{
		try{
			$this->arrSession = $this->controllerUserSession->get($strSessionHash, "user_session_hash");
			$this->arrUser = $this->controllerUser->get($this->arrSession["user_id"]);
		}
		catch(GeneralException $exc)
		{
			if($exc->getCode() !== GeneralException::PRODUCT_NOT_FOUND)
				throw $exc;
		}
	}

	function isValid()
	{
		if(
			array_key_exists("user_session_end_date", $this->arrSession)
			&& array_key_exists("user_session_hash", $this->arrSession)
			&& array_key_exists("user_session_start_date", $this->arrSession)
			&& array_key_exists("user_session_terminated", $this->arrSession))
		{
			if($this->arrSession["user_session_terminated"] == 1)
				return false;

			$nEndTime = strtotime($this->arrSession["user_session_end_date"]);
			if($nEndTime > time() && strlen($this->arrSession["user_session_hash"]))
			{
				$nExtendTime = 1800;
				if($nEndTime - $nExtendTime < time())
				{
					$this->arrSession["user_session_end_date"] = date("Y-m-d H:i:s", time() + $nExtendTime);
					$this->arrSession = $this->controllerUserSession->edit($this->arrSession["user_session_id"], $this->arrSession);
				}
				return true;
			}
		}
		return false;
	}
}