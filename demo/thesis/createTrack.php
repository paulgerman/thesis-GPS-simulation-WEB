<?php
function renderPageCreateTrack($arrUser)
{
	?>
	<input id="origin-input" class="form-control controls" type="text"
	       placeholder="Enter an origin location">

	<input id="destination-input" class="form-control controls" type="text"
	       placeholder="Enter a destination location">

	<div id="mode-selector" class="controls">
		<input type="radio" name="type" id="changemode-walking" checked="checked">
		<label for="changemode-walking">Walking</label>

		<input type="radio" name="type" id="changemode-transit">
		<label for="changemode-transit">Transit</label>

		<input type="radio" name="type" id="changemode-driving">
		<label for="changemode-driving">Driving</label>
	</div>

	<div id="floating-panel">
		<p>Total Distance: <span id="total"></span></p>
		<input class="form-control input-sm" type="text" id="track_name" placeholder="Track name"><br>
		<button class="btn-default btn-sm" id="next">VALUE</button>
		<button class="btn-default btn-sm" id="cancelEditing">Cancel vertex editing</button>
		<button class="btn-default btn-sm" id="saveRoute">Save route</button>
	</div>
	<div id="map"></div>
	<!-- Replace the value of the key parameter with your own API key. -->

	<script>
		new CreateRouteEngine();
	</script>
	<?php
}