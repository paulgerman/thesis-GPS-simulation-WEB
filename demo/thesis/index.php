<?php
$arrPages = array(
	"general" => array("GENERAL", "fa-home"),
	"tracks" => array("TRACKS", "fa-road"),
	"createTrack" => array("NEW TRACK", "fa-plus-square"),
	"simulations" => array("SIMULATIONS", "fa-map"),
	"createSimulation" => array("NEW SIMULATION", "fa-plus-square"),
);


header('Content-Type: text/html; charset=utf-8');
ob_start();
$nStart = microtime();
require_once("autoload.php");
require_once("config.php");
require_once(__DIR__.DIRECTORY_SEPARATOR."renderer.php");
require_once(__DIR__.DIRECTORY_SEPARATOR."Session.php");

$arrUser = array();
$session = new Session();
if(array_key_exists("session_man", $_COOKIE))
{
	$session->load($_COOKIE["session_man"]);
	if($session->isValid())
		$arrUser = $session->arrUser;
}
$strErrorMsg = "";
if(array_key_exists("email_reg", $_POST) && array_key_exists("password", $_POST))
{
    if(!filter_var($_POST["email_reg"], FILTER_VALIDATE_EMAIL))
        throw new GeneralException("Please don't");

    $userController = new \Controllers\ThesisUserController();
    try {
        $userController->create([
                "user_email" => $_POST["email_reg"],
                "user_password" => $_POST["password"],
                "user_first_name" => $_POST["first_name"],
                "user_last_name" => $_POST["last_name"],
            ]
        );
        $_POST["email"] = $_POST["email_reg"];
        $_GET["page"] = "general";
    }
    catch (GeneralException $exc)
    {
        $strErrorMsg = "Account already exists!";
    }

}

if(array_key_exists("email", $_POST) && array_key_exists("password", $_POST))
{
	try
	{
		$session->create($_POST["email"], $_POST["password"], (isset($_POST["remember"]) && $_POST["remember"] == 1) ? true : false);
	}
	catch(\GeneralException $exc)
	{
		if(
			$exc->getCode() !== GeneralException::PRODUCT_NOT_FOUND
			&& $exc->getCode() !== GeneralException::INVALID_PASSWORD
		)
			throw $exc;
	}
	if($session->isValid())
	{
		$session->storeCookie();
		$arrUser = $session->arrUser;
	}
}
$strPage = isset($_GET['page']) ? $_GET["page"]: "general";

?>
	<html>
	<head>
		<meta charset="UTF-8">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
		<script
				src="https://code.jquery.com/jquery-3.2.1.min.js"
				integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
				crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<link href="css/style.css" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
		      integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
		      crossorigin="anonymous">
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA055oxUbQVDN9ebODwE9JNY1Nz0J9cITE&libraries=places,geometry"></script>
		<script src="js/DeleteMenu.js"></script>
		<script src="js/AutocompleteDirectionsHandler.js"></script>
		<script src="js/utils.js"></script>
		<script src="js/Route.js"></script>
		<script src="js/Simulation.js"></script>
		<script src="js/CreateRouteEngine.js"></script>
		<script src="js/DisplayRouteEngine.js"></script>
		<script src="js/CreateSimulationEngine.js"></script>
		<script src="js/SimulationDataPoint.js"></script>
		<script src="../js/jquery.jsonrpc.js"></script>
		<script src="js/JsonRPCClient.js"></script>
		<script src="js/notify.min.js"></script>
		<script src="js/jquery.qrcode.min.js"></script>

		<script>
			<?="var arrUser = ".json_encode($arrUser).";\n";?>
		</script>
	</head>

	<body>
		<div class="parent">
			<div class="left-menu">
				<div class="left-menu-top">
					<img class="img-circle" src="<?=(@$arrUser["user_pic"] != ""?$arrUser["user_pic"]:'img/default_avatar.png')?>">
					<?php
					if(count($arrUser))
						echo $arrUser["user_first_name"]." ".$arrUser["user_last_name"];
					?>
				</div>
				<div class="left-menu-content">
					<?php
					foreach($arrPages as $strValue => $arrPage)
					{
						echo '
					<a href="?page='.$strValue.'">
						<div class="left-menu-item '.($strPage==$strValue?"active":"").'">
							<i class="fa '.$arrPage[1].'" aria-hidden="true"></i>
							'.$arrPage[0].'
						</div>
					</a>';
					}
					?>
				</div>
			</div>
			<div style="width: 200px; height: 100%; float:left;"></div>

			<div class="content">
				<div class="content-top">
					<div style="float:left">
						<img class="img-circle content-top-img" src="<?=(@$arrUser["user_pic"] != ""?$arrUser["user_pic"]:'img/default_avatar.png')?>">
						<div style="display: inline-block;">
							<div class="content-top-name">
								<?php
								if(count($arrUser))
									echo $arrUser["user_first_name"]." ".$arrUser["user_last_name"];
								?>
							</div>
						</div>
					</div>
					<div style="float:right; text-align: center;">
						<div class="menu-item-right">
							<a href="#">
								<img class="menu-image" src="img/icon_edit.png">
								<div class="menu-text-box">Support</div>
							</a>
						</div>
						<?php
						if(!count($arrUser))
						{
							?>
							<div class="menu-item-right">
								<a href="index.php?page=login&redirect=<?= urlencode($_SERVER['REQUEST_URI']) ?>">
									<img class="menu-image" src="img/icon_login.png">
									<div class="menu-text-box">Login</div>
								</a>
							</div>
							<?php
						}
						else
						{?>
							<div class="dropdown menu-item-right">
								<a href="#" class="dropdown-toggle" data-trigger="hover" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<img class="menu-image" src="img/icon_logout.png">
									<div class="menu-text-box">Account</div>
								</a>
								<ul class="dropdown-menu dropdown-menu-right">
									<?php
									if($arrUser["user_rank"] == "admin")
										echo '
									<li><a target="_blank" href="editUsers.php">Edit users</a></li>
									';
									?>
									<li><a href="index.php?page=panel">User panel</a></li>
									<li><a href="index.php?page=logout&redirect=<?=urlencode($_SERVER['REQUEST_URI'])?>">Log out</a></li>
								</ul>
							</div>
							<?php
						}?>

					</div>
					<div class="spacer"></div>
				</div>
				<div style="position: relative;">
				<?php
				switch($strPage)
				{
					case "login":
						echo renderLogin($arrUser);
						break;
					case "register":
						echo renderRegister($arrUser, $strErrorMsg);
						break;
					case "panel":
						if(!count($arrUser))
							echo renderLogin($arrUser);
						else
							echo renderPanel($arrUser);
						break;
					case "createTrack":
						if(!count($arrUser))
							echo renderLogin($arrUser);
						else
						{
							require_once("createTrack.php");
							renderPageCreateTrack($arrUser);
						}
						break;
					case "tracks":
						if(!count($arrUser))
							echo renderLogin($arrUser);
						else
						{
							require_once("tracks.php");
							renderPageTracks($arrUser);
						}
						break;
					case "simulations":
						if(!count($arrUser))
							echo renderLogin($arrUser);
						else
						{
							require_once("simulations.php");
							renderPageSimulations($arrUser);
						}
						break;
					case "createSimulation":
						if(!count($arrUser))
							echo renderLogin($arrUser);
						else
						{
							require_once("createSimulation.php");
							renderPageCreateSimulation($arrUser);
						}
						break;
					case "logout":
						$session->delete();
						$session->deleteCookie();
						header('Location: '.$_GET["redirect"]);
						exit();
						break;
					default:
                        if(!count($arrUser))
                            echo renderLogin($arrUser);
                        else
                        {
                            require_once("general.php");
                            renderPageGeneral($arrUser);
                        }
                        break;

				}

				?>
				</div>
			</div>
		</div>
	</body>
	</html>
<?php
ob_end_flush();