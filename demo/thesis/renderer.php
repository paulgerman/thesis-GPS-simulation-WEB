<?php

function renderLogin($arrUser)
{
	if(!empty($arrUser))
	{
		header('Location: '.(isset($_GET["redirect"])?$_GET["redirect"]:"?"));
		exit;
	}
	$strHTML = '
	<div class="panel">
	
		<div style="width:400px; margin: auto;">';
		if(empty($arrUser) && isset($_POST['email']))
			$strHTML .= '
			<div class="alert alert-warning alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Error!</strong> Wrong username or password!
			</div>';
		$strHTML .= '
	      <form class="form-signin" method="post">
	        <h2 class="form-signin-heading">Login</h2>
	        <label for="email" class="sr-only">Email</label>
	        <input name="email" class="form-control" placeholder="Email" required autofocus>
	        <label for="inputPassword" class="sr-only">Password</label>
	        <input type="password" name="password" class="form-control" placeholder="Password" required>
	        <div class="checkbox">
	          <label>
	            <input type="checkbox" name="remember" value="1">Remember me
	          </label>
	        </div>
	        <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button><br>
	        
	        <a href="?page=register">
	            <button class="btn btn-lg btn-success btn-block" type="button">Register new account</button>
            </a>
	      </form>
	    </div>
	</div>';
	return $strHTML;
}

function renderRegister($arrUser, $strErrorMsg)
{
    $strHTML = '
	<div class="panel">
	
		<div style="width:400px; margin: auto;">';
    if(strlen($strErrorMsg))
        $strHTML .= '
			<div class="alert alert-warning alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Error!</strong> '.$strErrorMsg.'
			</div>';
    $strHTML .= '
	      <form class="form-signin" method="post" onsubmit="return check();">
	        <h2 class="form-signin-heading">Register new account</h2>
	        <label for="email_reg" class="sr-only">Email</label>
	        <input name="email_reg" class="form-control" placeholder="Email" type="email" required autofocus>
	        <label for="first_name" class="sr-only">First name</label>
	        <input name="first_name" class="form-control" placeholder="First name" required>
	        <label for="last_name" class="sr-only">Last name</label>
	        <input name="last_name" class="form-control" placeholder="Last name" required>
	        <label for="inputPassword" class="sr-only">Password</label>
	        <input type="password" id="p1" name="password" class="form-control" placeholder="Password" required>
	        <label for="inputPassword" class="sr-only">Repeat Password</label>
	        <input type="password" id="p2"  name="password2" class="form-control" placeholder="Password repeat" required>
	        
	        <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
	      </form>
	    </div>
	</div>';
    $strHTML .= "
    <script>
        function check()
        {
            if(document.getElementById('p1').value !== document.getElementById('p2').value)
            {
                alert('Passwords must match!');
                return false;
            }
            return true;
        }
    </script>";
    return $strHTML;
}

function renderPanel($arrUser)
{
	if(empty($arrUser))
	{
		header('Location: ?');
		exit;
	}

	$nValidPw = 0;
	if(isset($_POST["passwordOld"]))
	{
		if($arrUser["user_password"] === md5($_POST["passwordOld"].$arrUser["user_salt"]))
		{

			if($_POST["passwordNew1"] === $_POST["passwordNew2"])
			{
				if(strlen($_POST["passwordNew1"]) >= 5)
				{

					$nValidPw = 1;
					$controllerUser = new \Controllers\ThesisUserController();
					$arrUser["user_password"] = md5($_POST["passwordNew1"].$arrUser["user_salt"]);
					$controllerUser->editInternal($arrUser["user_id"], $arrUser);
				}
				else
					$nValidPw = -3;
			}
			else
				$nValidPw = -2;
		}
		else
		{
			$nValidPw = -1;
		}
	}

	$strHTML = '
	<div style="width:400px; margin: auto;">';
	if($nValidPw === -1)
		$strHTML .= '
<div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Error!</strong> Incorrect Password!
</div>';
	if($nValidPw === -2)
		$strHTML .= '
<div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Error!</strong> New password does not match!
</div>';
	if($nValidPw === -3)
		$strHTML .= '
<div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Error!</strong> New password must contain minimum 5 characters!
</div>';
	if($nValidPw === 1)
		$strHTML .= '
<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Succes!</strong> Password changed!
</div>';
	$strHTML .= '
      <form class="form-signin" method="post">
        <h2 class="form-signin-heading">Change password</h2>
        <label for="inputPassword" class="sr-only">Current password</label>
        <input type="password" name="passwordOld" class="form-control" placeholder="Current password" required>
        <label for="inputPassword" class="sr-only">New password</label>
        <input type="password" name="passwordNew1" class="form-control" placeholder="New password" required>
        <label for="inputPassword" class="sr-only">Repeat new password</label>
        <input type="password" name="passwordNew2" class="form-control" placeholder="Repeat new password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Change</button>
      </form>
    </div>';
	return $strHTML;
}