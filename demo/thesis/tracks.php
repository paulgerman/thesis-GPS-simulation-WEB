<?php
function renderPageTracks($arrUser)
{
	?>
	<div class="panel">
		<div class="panel-top">
			<div class="panel-title">
				AVAILABLE TRACKS
			</div>
			<div class="panel-filter">
			</div>
		</div>
		<hr>
		<div class="panel-content" style="padding-left: 30px; padding-right: 30px;">
			<?php displayTracks($arrUser); ?>
		</div>
	</div>
	<?php
}

function displayTracks($arrUser, $strHrefStart = "?page=tracks&edit=")
{
	$controller = new \Controllers\ThesisTrackController();
	if(isset($_GET["delete"]))
		try {
			$arrTrack = $controller->get($_GET["delete"]);

			$controllerSimulation = new \Controllers\ThesisSimulationController();
			$arrSimulations = $controllerSimulation->get_all_internal(0,999999, " WHERE `track_id` = ".trdb()->quote($arrTrack["track_id"]));
			foreach($arrSimulations as $arrSimulation)
            {
                $controllerSimulation->delete($arrSimulation["simulation_id"]);
            }

			$controller->delete($_GET["delete"]);
		}
		catch(GeneralException $exc)
		{
			if($exc->getCode() !== GeneralException::PRODUCT_NOT_FOUND)
				throw $exc;
		}

	$arrTracks = $controller->get_all_internal(0, 99999, " WHERE `user_id` = ".trdb()->quote($arrUser["user_id"]));
	?>

<div class="row">
	<?php
    if(count($arrTracks) === 0)
    {
        echo "You didn't create any tracks yet!";
    }
	foreach($arrTracks as $arrTrack)
	{
		?>

		<a href="<?=$strHrefStart.$arrTrack["track_id"]?>">
			<div class="col-sm-6 col-md-4 track-preview">
				<div id="map<?=$arrTrack["track_id"]?>" class="map-preview">
				</div>

				<div class="track-name">
					<?=$arrTrack["track_name"]?>
                    <a onclick="return confirm('This will delete all associated simulations. Are you sure?')" href="?page=tracks&delete=<?=$arrTrack["track_id"]?>">[x]</a>
                </div>

				<script>
					new DisplayRouteEngine('map<?=$arrTrack["track_id"]?>', <?=$arrTrack["track_data_vertices"]?>);
				</script>
			</div>
		</a>

		<?php
	}
	?>
</div>

<?php
}