<?php
require_once("autoload.php");
require_once("config.php");
require_once(__DIR__.DIRECTORY_SEPARATOR."Session.php");
$arrUser = array();
$bLoggedIn = false;
$session = new Session();
if(array_key_exists("session_man", $_COOKIE))
{
	$session->load($_COOKIE["session_man"]);
	if($session->isValid())
	{
		$arrUser = $session->arrUser;
		$bLoggedIn = true;
	}
}
if(!$bLoggedIn || array_key_exists("user_rank", $arrUser) && $arrUser['user_rank'] !== "admin")
{
	header('Location: index.php?page=login');
	exit();
}
?>
<head>
	<meta charset="UTF-8">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>

	<script src="../js/sortable.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.13/b-1.2.3/b-colvis-1.2.3/b-flash-1.2.3/b-html5-1.2.3/cr-1.3.2/fc-3.2.2/fh-3.1.2/datatables.min.css"/>
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.13/b-1.2.3/b-colvis-1.2.3/b-flash-1.2.3/b-html5-1.2.3/cr-1.3.2/fc-3.2.2/fh-3.1.2/datatables.min.js"></script>

	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
	<script src="../js/jquery.jsonrpc.js"></script>
	<script src="js/JsonRPCClient.js"></script>
	<script src="../js/Renderer/Renderer.js"></script>
	<script src="../js/Renderer/ThesisUser.js"></script>
	<script src="../js/Renderer/Elements/BaseElement.js"></script>

	<script src="../js/Renderer/Elements/Table.js"></script>
	<script src="../js/Renderer/Elements/Reorder.js"></script>
	<script src="../js/Renderer/Utils.js"></script>
	<script src="../js/Renderer/Elements/Form.js"></script>
	<script src="../js/Renderer/Elements/Form/BaseFormElement.js"></script>
	<script src="../js/Renderer/Elements/Form/Image.js"></script>
	<script src="../js/Renderer/Elements/Form/Multiselect.js"></script>
	<script src="../js/Renderer/Elements/Form/MultiImage.js"></script>
	<script src="../js/Renderer/Elements/Form/ColorRow.js"></script>
	<script src="../js/Renderer/Elements/Form/BootstrapWYSIWYG.js"></script>
</head>
<body>
<div class="container" id="container">
</div>
<div class="container">
</div>

<script>
	<?php
	setcookie("auth", "1235", 0, '/');

	require_once ("autoload.php");
	require_once ("functions/functions.php");
	echo 'var nMaxFileSize = '.getMaximumFileUploadSize().';'."\n";
	echo 'var nMaxImageSize = '.getMaximumFileUploadSize().';'."\n";

	echo "var arrConfigUser = ".json_encode(retrieveObjConfig(\Objects\ThesisUser::class)).";\n";
	echo "var arrUser = ".json_encode($arrUser).";\n";
	?>

	var renderer = new ThesisUser(document.getElementById("container"));
	renderer.init();
</script>

</body>
