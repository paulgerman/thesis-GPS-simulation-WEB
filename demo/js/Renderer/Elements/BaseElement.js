class BaseElement{

	static get classes()
	{
		return {
			Multiselect,
			ColorRow,
			Image,
			MultiImage,
			File,
			BootstrapWYSIWYG
		}
	};

	/**
	 *
	 * @param {Element} elParent
	 * @param arrData
	 * @param arrOptions
	 */
	constructor(elParent, arrData, arrOptions)
	{
		this.elParent = elParent;
		this.arrData = arrData;
		this.arrOptions = arrOptions;
	}

	render()
	{

	}

	removeItem()
	{

	}

	addItem()
	{

	}
}