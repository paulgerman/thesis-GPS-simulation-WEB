class Table extends BaseElement{
	constructor(elParent, arrData, arrOptions, arrActions = [], arrHiddenKeys = [], arrSelectValues)
	{
		super(elParent, arrData, arrOptions);
		this.arrActions = arrActions;
		this.arrHiddenKeys = arrHiddenKeys;
		this.arrSelectValues = arrSelectValues;
	}


	render()
	{
		let elTable = document.createElement("table");
		this.elTable = elTable;
		elTable.className = "table table-hover table-striped";

		let arrColumns = [];
		let columnDefs = [];
		for(let strKey in this.arrOptions["cols"])
		{
			let arrOptions = this.arrOptions["cols"][strKey];
			if(!isString(arrOptions) && this.arrHiddenKeys.indexOf(strKey) === -1)
			{
				let arrCol = {
					title: arrOptions["displayName"],
					data: strKey,
					className: strKey
				};
				arrColumns.push(arrCol);


				if(arrOptions["internalType"] !== undefined)
				{
					if(BaseElement.classes[arrOptions["internalType"]] !== undefined)
					{
						arrCol["render"] = (data, type, full, meta) => {
							let api = new $.fn.dataTable.Api(meta["settings"]);
							let elTmp = document.createElement("div");
							let baseElement = new BaseElement.classes[arrOptions["internalType"]](elTmp, full[strKey], this.arrSelectValues[strKey]);
							baseElement.render();
							return elTmp.innerHTML;
						};
						/*columnDefs.push({
							targets: this.getColumnIndexesWithClass( arrColumns, strKey ),
							createdCell: (td, cellData, rowData, row, col) => {
								console.log("MIAU");

								let baseElement = new BaseElement.classes[arrOptions["internalType"]](td, cellData, this.arrSelectValues[strKey]);
								baseElement.render();
							}
						});*/
					}
					else
					{
						arrCol["render"] = (data, type, full, meta) => {
							return 'penis';
						};
					}
				}
			}
		}

		if(this.arrActions.length)
		{
			arrColumns.push(
				{
					title: "---",
					data: null,
					className: "actions",
					defaultContent: "bug :("
				}
			);

			columnDefs.push({
				targets: this.getColumnIndexesWithClass(arrColumns, "actions"),
				createdCell: (td, cellData, rowData, row, col) => {
					$(td).empty();
					this.generateActions(td, rowData[this.arrOptions["indexKey"]]);
				}
			 });
		}
		this.elParent.appendChild(elTable);

		this.dataTable = $(elTable).DataTable({
			stateSave: true,
			stateDuration: 60 * 60 * 24 * 31,
			dom: "Bfrltip",
			buttons: [
				'colvis'
			],
			colReorder: true,
			fixedHeader: true,
			columns: arrColumns,
			data: [],
			"columnDefs" : columnDefs
		});

		for(let i in this.arrData)
		{
			this.addItem(this.arrData[i]);
		}
		this.dataTable.draw();

		this.dataTable.on( 'column-reorder', ( e, settings, details ) =>  {
			this.dataTable.clear();
			for(let i in this.arrData)
			{
				this.addItem(this.arrData[i]);
			}
			this.dataTable.draw(false);
		});

		/*let elButtonTens = document.createElement("button");
		elButtonTens.type = "button";
		elButtonTens.className = "btn btn-default";
		elButtonTens.addEventListener("click", () => {
			this.dataTable.row.add(this.arrData[215]).draw();
		});
		elButtonTens.appendChild(document.createTextNode("get state"));
		this.elParent.appendChild(elButtonTens);*/


	}

	render2()
	{
		let elTable = document.createElement("table");
		this.elTable = elTable;
		elTable.className = "table table-hover table-striped";

		let elTHead = document.createElement("thead");
		let elTR = document.createElement("tr");
		elTHead.appendChild(elTR);

		for(let strKey in this.arrOptions["cols"])
		{
			let arrOptions = this.arrOptions["cols"][strKey];
			if(!isString(arrOptions) && this.arrHiddenKeys.indexOf(strKey) === -1)
			{
				let elTH = document.createElement("th");
				elTH.appendChild(document.createTextNode(arrOptions["displayName"]));
				elTR.appendChild(elTH);
			}
		}

		if(this.arrActions.length)
		{
			let elTH = document.createElement("th");
			elTH.appendChild(document.createTextNode("---"));
			elTR.appendChild(elTH);
		}
		elTHead.appendChild(elTR);
		elTable.appendChild(elTHead);

		let elTBody = document.createElement("tbody");

		for(let i in this.arrData)
		{
			this.addItem(this.arrData[i], elTBody);
		}

		elTable.appendChild(elTBody);
		this.elParent.appendChild(elTable);

		this.dataTable = $(elTable).DataTable({
			"stateSave": true,
			"stateDuration": 60 * 60 * 24 * 31,
			"dom": "Bfrltip",
			"buttons": [
				'colvis'
			],
			"colReorder": true,
			"fixedHeader": true,
		});
	}

	removeItem()
	{

	}

	addRow(arrRow)
	{
		console.log(this.dataTable);
		//this.dataTable.row.add(arrRow).draw( false );
	}

	addItem(arrObject)
	{
		let arrData = {};
		let arrEnumKeys = this.arrOptions["enumKeys"] === undefined ? [] : this.arrOptions["enumKeys"];

		for(let strKey in this.arrOptions["cols"])
		{
			if(!this.arrOptions["cols"].hasOwnProperty(strKey))
				continue;

			let arrOptions = this.arrOptions["cols"][strKey];
			if(isString(arrOptions) || this.arrHiddenKeys.indexOf(strKey) !== -1)
				continue;


			let nSizeLimit = 999999;
			if(arrOptions["maxDisplaySize"] !== undefined)
				nSizeLimit = arrOptions["maxDisplaySize"];

			if(arrOptions["displayOptions"] !== undefined)
			{
				let arrDisplayOptions = arrayCombine(arrEnumKeys[strKey], arrOptions["displayOptions"]);

				arrData[strKey] = arrDisplayOptions[arrObject[strKey]];
			}
			else if(Array.isArray(arrObject[strKey]) && arrOptions["internalType"] === undefined)
			{
				arrData[strKey] = arrObject[strKey].join(", ");
				arrData[strKey] = arrObject[strKey];
			}
			else
			{
				arrData[strKey] = arrObject[strKey];
			}
		}

		this.dataTable.row.add(arrData);
	}

	addItem2(arrObject, elParent)
	{
		let arrEnumKeys = this.arrOptions["enumKeys"] === undefined ? [] : this.arrOptions["enumKeys"];

		let elTR = document.createElement("tr");

		for(let strKey in this.arrOptions["cols"])
		{
			if(!this.arrOptions["cols"].hasOwnProperty(strKey))
				continue;

			let arrOptions = this.arrOptions["cols"][strKey];
			if(isString(arrOptions) || this.arrHiddenKeys.indexOf(strKey) !== -1)
				continue;

			let elTD = document.createElement("td");
			elTR.appendChild(elTD);

			if(arrOptions["internalType"] !== undefined)
			{
				if(BaseElement.classes[arrOptions["internalType"]] !== undefined)
				{
					let baseElement = new BaseElement.classes[arrOptions["internalType"]](elTD, arrObject[strKey], this.arrSelectValues[strKey]);
					baseElement.render();
				}
				else
				{
					elTD.appendChild(document.createTextNode("SAMPLE FOR " + arrOptions["internalType"]));
					//throw ("undefined class" + arrOptions["internalType"]);
				}
			}
			else
			{
				let nSizeLimit = 999999;
				if(arrOptions["maxDisplaySize"] !== undefined)
					nSizeLimit = arrOptions["maxDisplaySize"];

				if(arrOptions["displayOptions"] !== undefined)
				{
					let arrDisplayOptions = arrayCombine(arrEnumKeys[strKey], arrOptions["displayOptions"]);

					elTD.appendChild(document.createTextNode(arrDisplayOptions[arrObject[strKey]]));
				}
				else if(Array.isArray(arrObject[strKey]))
				{
					elTD.appendChild(document.createTextNode(arrObject[strKey].join(", ")));
				}
				else
				{
					elTD.appendChild(document.createTextNode(arrObject[strKey]));
				}

			}
		}

		if(this.arrActions.length)
		{
			let elTD = document.createElement("td");
			for(let i in this.arrActions)
			{
				let arrAction = this.arrActions[i];
				let elButton = document.createElement("button");
				elButton.type = "button";
				elButton.className = "btn btn-primary btn-sm";
				elButton.style = "margin-bottom:2px;";
				elButton.appendChild(document.createTextNode(arrAction["text"]));

				if(arrAction["type"] === "delete")
				{
					elButton.addEventListener('click', (event) => {
						let self = event.target;
						self.disabled = true;
						call(
							arrAction["method"],
							[arrObject[this.arrOptions["indexKey"]]],
							() => {
								$(self).parents('tr').fadeOut(
									'fast',
									'swing',
									() => {
										this.dataTable
											.row( $(self).parents('tr') )
											.remove()
											.draw();
										if(arrAction["callback"] !== undefined)
										{
											arrAction["callback"](arrObject[this.arrOptions["indexKey"]]);
										}
									}
								)
							},
							() => {
								self.disabled = false;
							}
						);

					});
				}
				else if(arrAction["callback"] !== undefined)
				{
					elButton.addEventListener('click', (event) => {
						arrAction["callback"](arrObject[this.arrOptions["indexKey"]]);
					});
				}

				elTD.appendChild(elButton);
				elTD.appendChild(document.createElement("br"));
			}
			elTR.appendChild(elTD);
		}

		elParent.appendChild(elTR);
	}


	getColumnIndexesWithClass( columns, className ) {
		let indexes = [];
		$.each( columns, function( index, columnInfo ) {
			// note: doesn't attempt to support multiple class names on a column
			if( columnInfo.className == className ) {
				indexes.push( index );
			}
		} );

		return indexes;
	}

	generateActions(elParent, nKey)
	{
		if(this.arrActions.length)
		{
			let elTD = document.createElement("td");
			for(let i in this.arrActions)
			{
				let arrAction = this.arrActions[i];
				let elButton = document.createElement("button");
				elButton.type = "button";
				elButton.className = "btn btn-primary btn-sm";
				elButton.style = "margin-bottom:2px;";
				elButton.appendChild(document.createTextNode(arrAction["text"]));

				if(arrAction["type"] === "delete")
				{
					elButton.addEventListener('click', (event) => {
						let self = event.target;
						self.disabled = true;
						call(
							arrAction["method"],
							[nKey],
							() => {
								$(self).parents('tr').fadeOut(
									'fast',
									'swing',
									() => {
										this.dataTable
											.row( $(self).parents('tr') )
											.remove()
											.draw(false);
										if(arrAction["callback"] !== undefined)
										{
											arrAction["callback"](nKey);
										}
									}
								)
							},
							() => {
								self.disabled = false;
							}
						);

					});
				}
				else if(arrAction["callback"] !== undefined)
				{
					elButton.addEventListener('click', (event) => {
						arrAction["callback"](nKey);
					});
				}

				elTD.appendChild(elButton);
				elTD.appendChild(document.createElement("br"));
			}
			elParent.appendChild(elTD);
		}
	}
}