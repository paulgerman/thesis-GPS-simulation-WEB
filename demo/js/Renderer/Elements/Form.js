class Form extends BaseElement{
	constructor(elParent, arrData, arrOptions, arrHiddenKeysValues, bAllowAdd)
	{
		super(elParent, arrData, arrOptions);
		this.arrHiddenKeysValues = (typeof arrHiddenKeysValues === "object")? arrHiddenKeysValues : {};
		this.bAllowAdd = (bAllowAdd === undefined) ? true : bAllowAdd;
		this.arrFormInputs = {};
		this.arrFormCustomElements = {};
		this.bEditMode = false;
		this.arrPromisesSubmit = [];
	}

	render()
	{
		let elForm = document.createElement("form");
		elForm.className = "form-horizontal";
		elForm.onsubmit = () => {
			let arrStateData = this.getState(true).arrData;

			if(this.fnValidation)
				if(!this.fnValidation(arrStateData))
					return false;

			if(this.bEditMode)
			{
				this.elButtonSave.disabled = true;
				this.elButtonSave.firstElementChild.classList.remove("hidden");
			}
			else
			{
				this.elButtonAdd.disabled = true;
				this.elButtonAdd.firstElementChild.classList.remove("hidden");
			}


			$.when.apply($,this.arrPromisesSubmit).then((...args) => {
				if(args[0] !== undefined)
					for(let i in args)
						arrStateData[args[i]["key"]] = args[i]["value"];

				if(this.bEditMode)
					this.fnEditCallback(arrStateData[this.arrData["indexKey"]], this.filterState(arrStateData));
				else
					this.fnAddCallback(this.filterState(arrStateData));

				this.arrPromisesSubmit = [];
			});

			return false;
		};
		if(!this.bAllowAdd)
			elForm.style.display = "none";
		this.elForm = elForm;

		for(let strKey in this.arrHiddenKeysValues)
		{
			let elInput = document.createElement("input");
			elInput.type = "hidden";
			elInput.value = this.arrHiddenKeysValues[strKey];
			elForm.appendChild(elInput);
		}

		for(let strKey in this.arrData["cols"])
		{
			if(
				this.arrHiddenKeysValues[strKey] === undefined
				&& this.arrData["readOnlyKeys"].indexOf(strKey) === -1
			)
			this.addItem(this.arrData["cols"][strKey], strKey, elForm);
		}

		let elButtonAdd = document.createElement("button");
		elButtonAdd.type = "submit";
		elButtonAdd.className = "btn btn-default";
		elButtonAdd.appendChild(createElement("span", {class:"glyphicon glyphicon-refresh glyphicon-refresh-animate hidden"}));
		elButtonAdd.appendChild(document.createTextNode("Add"));
		elButtonAdd.style.marginRight = "10px";

		this.elButtonAdd = elButtonAdd;

		let elButtonSave = document.createElement("button");
		elButtonSave.type = "submit";
		elButtonSave.className = "btn btn-default";
		elButtonSave.appendChild(createElement("span", {class:"glyphicon glyphicon-refresh glyphicon-refresh-animate hidden"}));
		elButtonSave.appendChild(document.createTextNode("Save"));
		elButtonSave.style.marginRight = "10px";


		elButtonSave.style.display = "none";
		this.elButtonSave = elButtonSave;

		let elButtonClose = document.createElement("button");
		elButtonClose.type = "button";
		elButtonClose.className = "btn btn-default";
		elButtonClose.appendChild(document.createTextNode("Close edit"));
		elButtonClose.style.display = "none";
		elButtonClose.addEventListener("click", () => {
			if(this.fnCloseEditCallback)
				this.fnCloseEditCallback();
			else
				this.resetState();
		});

		this.elButtonClose = elButtonClose;

		elForm.appendChild(elButtonAdd);
		elForm.appendChild(elButtonSave);
		elForm.appendChild(elButtonClose);

		this.elKeyDiv.style.display = "none";
		this.elParent.appendChild(elForm);
		this.resetState();
	}

	removeItem()
	{

	}

	addItem(arrItem, strKey, elParent)
	{
		if(isString(arrItem))
		{
			$(elParent).append(arrItem);
			return;
		}

		let elDiv = document.createElement("div");
		elDiv.className = "form-group";
		let strDisplayName = strKey;
		if(arrItem["displayName"] !== undefined)
			strDisplayName = arrItem["displayName"];

		let elLabelDiv = createElement("div", {class: "col-sm-2"});
		let elLabel = document.createElement("label");
		elLabel.className = "control-label";
		elLabel.style.width = "100%";
		if(arrItem["internalAttributes"] && arrItem["internalAttributes"]["required"] === true)
			elLabel.appendChild(document.createTextNode(strDisplayName+"*"));
		else
			elLabel.appendChild(document.createTextNode(strDisplayName));
		elLabelDiv.appendChild(elLabel);
		elDiv.appendChild(elLabelDiv);


		if(arrItem["tooltip"] !== undefined)
		{
			elLabelDiv.appendChild(createElement("br"));
			elLabelDiv.appendChild(createElement("span", {style: "font-size: 11px;text-align: right;width: 100%;display: block;"}, " " + arrItem["tooltip"]));
		}

		let elInputDiv = createElement("div", {class: "col-sm-10"});
		elDiv.appendChild(elInputDiv);
		if(arrItem["internalType"] !== undefined)
		{
			arrItem["internalType"] = capitalizeFirstLetter(arrItem["internalType"]);

			if(BaseElement.classes[arrItem["internalType"]] !== undefined)
			{
				let baseElement = new BaseElement.classes[arrItem["internalType"]](elInputDiv, [], arrItem, this, strKey);
				baseElement.renderEdit();
				this.arrFormCustomElements[strKey] = baseElement;
			}
			else
			{
				elInputDiv.appendChild(document.createTextNode("SAMPLE FOR " + arrItem["internalType"]));
				//throw ("undefined class" + arrOptions["internalType"]);
			}
		}
		else
		{
			this.arrFormInputs[strKey] = this.defaultElementEdit(strKey, elInputDiv);
		}

		if(strKey === this.arrData["indexKey"])
			this.elKeyDiv = elDiv;

		elParent.appendChild(elDiv);
	}

	defaultElementEdit(strKey, elParent)
	{
		let arrCol = this.arrData["cols"][strKey];
		let arrSelectOptions = [];
		let arrEnumValues = undefined;
		if(this.arrData["enumKeys"] !== undefined)
			arrEnumValues = this.arrData["enumKeys"][strKey];

		if(arrCol["type"] === undefined)
		{
			arrCol["type"] = "text";
		}

		if(arrCol["type"] === "select")
		{
			if (arrEnumValues !== undefined && this.arrOptions[strKey] === undefined)
			{
				arrCol["type"] = "select";
				for (let i in arrEnumValues)
				{
					let strText;
					if (arrCol["displayOptions"] !== undefined)
						strText = arrCol["displayOptions"][i];
					else
						strText = arrEnumValues[i];

					arrSelectOptions.push({
						"text": strText,
						"value": arrEnumValues[i],
					});
				}
			}
		}

		let arrElementOptions = {};
		let elSelect = undefined;
		let elInput;
		switch(arrCol["type"])
		{
			case "select":
			{
				if (arrSelectOptions.length === 0)
				{
					if (this.arrOptions[strKey] === undefined)
						throw "You haven't defined the select options for key " + strKey;
					arrSelectOptions = this.arrOptions[strKey];
				}

				arrElementOptions = {
					"class": "form-control",
				};

				if (array_key_exists("internalAttributes", arrCol))
				{
					arrElementOptions = $.extend({}, arrCol["internalAttributes"], arrElementOptions);
				}
				elSelect = createElement("select", arrElementOptions);

				for (let i in arrSelectOptions)
				{
					this.recursiveSelectDisplay(arrSelectOptions[i], elSelect);
				}
				elParent.appendChild(elSelect);
				elInput = elSelect;
				break;
			}
			case "textarea":
			{
				arrElementOptions =	{
					rows: 5,
					class: "form-control"
				};

				if (array_key_exists("internalAttributes", arrCol))
				{
					arrElementOptions = $.extend({}, arrCol["internalAttributes"], arrElementOptions);
				}

				elInput = createElement(
					"textarea",
					arrElementOptions
				);
				elParent.appendChild(elInput);
				break;
			}
			case "multiselect":
			{
				if (arrSelectOptions.length === 0)
				{
					if (this.arrOptions[strKey] === undefined)
						console.error("You haven't defined the select options for key " + strKey);
					arrSelectOptions = this.arrOptions[strKey];
				}

				arrElementOptions = {
					style: "width:100%",
					class: "form-control",
					multiple: "multiple",
				};


				if (array_key_exists("internalAttributes", arrCol))
				{
					arrElementOptions = $.extend({}, arrCol["internalAttributes"], arrElementOptions);
				}

				elSelect = createElement(
					"select",
					arrElementOptions
				);

				for (let i in arrSelectOptions)
				{
					console.log(arrSelectOptions[i]);
					this.recursiveSelectDisplay(arrSelectOptions[i], elSelect);
				}

				elParent.appendChild(elSelect);

				elInput = elSelect;
				break;
			}
			case "text":
			{
				arrElementOptions = {
					class: "form-control"
				};

				if (array_key_exists("internalAttributes", arrCol))
				{
					arrElementOptions = $.extend({}, arrCol["internalAttributes"], arrElementOptions);
				}

				elInput = createElement(
					"input",
					arrElementOptions
				);
				if (strKey === this.arrData["indexKey"])
					elInput.disabled = true;

				elParent.appendChild(elInput);
				break;
			}
			case "date":
			{
				arrElementOptions = {
					class: "form-control",
					type: "date"
				};

				if (array_key_exists("internalAttributes", arrCol))
				{
					arrElementOptions = $.extend({}, arrCol["internalAttributes"], arrElementOptions);
				}

				elInput = createElement(
					"input",
					arrElementOptions
				);
				if (strKey === this.arrData["indexKey"])
					elInput.disabled = true;

				elParent.appendChild(elInput);
				break;
			}
			default:
			{
				console.error("Type "+arrCol["type"]+" is not defined");
			}
		}
		return elInput;
	}


	recursiveSelectDisplay(arrItemOptions, elParent)
	{
		if(array_key_exists("type", arrItemOptions) && arrItemOptions["type"] === "optgroup")
		{
			let elOptGroup = createElement("optgroup", {
				"label" : arrItemOptions["text"],
			});

			for(let i in arrItemOptions["data"])
			{
				this.recursiveSelectDisplay(arrItemOptions["data"][i], elOptGroup);
			}
			elParent.appendChild(elOptGroup);
		}
		else
		{
			let elOption = createElement(
				"option",
				{
					"value" : arrItemOptions["value"],
				},
				arrItemOptions["text"]
			);

			if(array_key_exists("disabled", arrItemOptions))
			{
				elOption.disabled = true;
			}
			elParent.appendChild(elOption);
		}
	}

	loadState(arrState)
	{
		if(arrState.bEditMode === true && this.bEditMode === false)
			this.arrSavedState = this.getState();

		this.bEditMode = arrState.bEditMode;

		let arrData = arrState.arrData;
		for(let strKey in arrData)
		{
			if(array_key_exists(strKey, this.arrFormInputs))
			{
				this.arrFormInputs[strKey].value = arrData[strKey];
			}

			if(array_key_exists(strKey, this.arrFormCustomElements))
			{
				this.arrFormCustomElements[strKey].updateValue(arrData[strKey]);
			}
		}

		if(arrState.bEditMode)
		{
			this.elKeyDiv.style.display = "block";
			this.elButtonAdd.style.display = "none";
			this.elButtonSave.style.display = "inline-block";
			this.elButtonClose.style.display = "inline-block";
		}
		else
		{
			this.elKeyDiv.style.display = "none";
			this.elButtonAdd.style.display = "inline-block";
			this.elButtonSave.style.display = "none";
			this.elButtonClose.style.display = "none";
		}

		if(!this.bAllowAdd)
			if(this.bEditMode)
				this.elForm.style.display = "block";
			else
				this.elForm.style.display = "none";

		this.elButtonSave.firstElementChild.classList.add("hidden");
		this.elButtonAdd.firstElementChild.classList.add("hidden");
	}

	resetState()
	{
		if(this.arrSavedState !== undefined)
			this.loadState(this.arrSavedState);
		else
		{
			for (let strKey in this.arrFormInputs)
			{
				if(this.arrFormInputs[strKey].tagName === 'SELECT')
					this.arrFormInputs[strKey].selectedIndex = 0;
				else
					this.arrFormInputs[strKey].value = "";
			}

			for (let strKey in this.arrFormCustomElements)
			{
				this.arrFormCustomElements[strKey].updateValue("");
			}
		}

		this.elKeyDiv.style.display = "none";

		this.elButtonAdd.style.display = "inline-block";
		this.elButtonSave.style.display = "none";
		this.elButtonClose.style.display = "none";

		this.elButtonSave.disabled = false;
		this.elButtonAdd.disabled = false;
		this.elButtonSave.firstElementChild.classList.add("hidden");
		this.elButtonAdd.firstElementChild.classList.add("hidden");

		this.bEditMode = false;

	}

	getState(bFinalState)
	{
		let arrObject = {};

		for(let strKey in this.arrFormInputs)
		{
			if(this.arrData["cols"][strKey]["ghost"] === undefined)
				arrObject[strKey] = this.arrFormInputs[strKey].value;
		}

		for(let strKey in this.arrFormCustomElements)
		{
			if(this.arrData["cols"][strKey]["ghost"] === undefined)
			{
				if(bFinalState)
				{
					let promise = this.arrFormCustomElements[strKey].prepareSubmit();
					if(promise)
						this.arrPromisesSubmit.push(promise);
				}

				arrObject[strKey] = this.arrFormCustomElements[strKey].getValue();
			}
		}

		for(let strKey in this.arrHiddenKeysValues)
		{
			arrObject[strKey] = this.arrHiddenKeysValues[strKey];
		}

		return {arrData: arrObject, bEditMode: this.bEditMode};
	}

	filterState(arrState)
	{
		for(let i in arrState)
		{
			if(arrState[i] === null)
				delete arrState[i];
		}
		return arrState;
	}

	setValidation(fnValidation)
	{
		this.fnValidation = fnValidation;
	}
}