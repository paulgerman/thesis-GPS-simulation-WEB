class Reorder extends BaseElement{

	constructor(elParent, arrData, arrOptions, strDisplayKey, strSortKey, fnCallback)
	{
		super(elParent, arrData, arrOptions);
		this.strDisplayKey = strDisplayKey;
		this.arrDivs = {};
		this.fnCallback = fnCallback;
		this.strSortKey = strSortKey;

		this.arrData = this.valuesToArray(this.arrData);
		this.arrData.sort(this.sortItems.bind(this));
	}

	render()
	{
		let elDiv = document.createElement("div");
		elDiv.className = "list-group";
		this.elDiv = elDiv;
		for(let i in this.arrData)
		{
			this.addItem(this.arrData[i], elDiv);
		}

		this.elParent.appendChild(elDiv);

		let elButton = document.createElement("button");
		elButton.type = "button";
		elButton.className = "btn btn-default";
		elButton.setAttribute("style", "transition: all 0.5s ease; width: 136px;");

		let elTextNode = document.createTextNode("Salveaza ordinea");
		elButton.appendChild(elTextNode);

		elButton.addEventListener("click", () => {
			this.fnCallback(this.sortable.toArray(), this.strSortKey);
			elTextNode.nodeValue = "SALVAT";
			elButton.classList.remove("btn-default");
			elButton.classList.add("btn-success");
			setTimeout(() => {
				elTextNode.nodeValue = "Salveaza ordinea";
				elButton.classList.remove("btn-success");
				elButton.classList.add("btn-default");
			}, 1500);
		});
		this.elParent.appendChild(createElement("h5", {style: "color: #333333"}, "Trageti elementele in ordinea dorita si apasati butonul de salvare cand ati terminat aranjarea."));

		this.elParent.appendChild(elButton);

		this.sortable = Sortable.create(elDiv, {
			handle: '.move-item',
			animation: 150
		});
	}

	removeItem(nID)
	{
		this.arrDivs[nID].parentNode.removeChild(this.arrDivs[nID]);
		delete(this.arrDivs[nID]);
	}

	addItem(arrItem, elParent)
	{
		let elDiv = document.createElement("div");
		elDiv.className = "list-group-item move-item";
		elDiv.setAttribute("style", "cursor: pointer;");
		elDiv.setAttribute("data-id", arrItem[this.arrOptions]);

		let elSpan = document.createElement("span");
		elSpan.className = "glyphicon glyphicon-move";
		elSpan.setAttribute("aria-hidden", true);
		elDiv.appendChild(elSpan);
		elDiv.appendChild(document.createTextNode("ID" + arrItem[this.arrOptions] + ": " + arrItem[this.strDisplayKey]));
		elParent.appendChild(elDiv);

		this.arrDivs[arrItem[this.arrOptions]] = elDiv;
	}

	valuesToArray(obj)
	{
		return Object.keys(obj).map(function (key) { return obj[key]; });
	}

	sortItems(a, b)
	{
		if(parseInt(a[this.strSortKey]) < parseInt(b[this.strSortKey])) return -1;
		if(parseInt(a[this.strSortKey]) > parseInt(b[this.strSortKey])) return 1;
		if(a[this.arrOptions] < b[this.arrOptions]) return -1;
		if(a[this.arrOptions] > b[this.arrOptions]) return 1;
		return 0;
	}
}