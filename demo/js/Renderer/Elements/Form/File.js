class File extends BaseFormElement{
	/**
	 * @param {Element} elParent
	 * @param arrData
	 * @param arrOptions
	 * @param {Form} form
	 * @param {String|undefined} strKey
	 */
	constructor(elParent, arrData, arrOptions, form, strKey)
	{
		super(elParent, arrData, arrOptions, form, strKey);
		this.strFile = "";
	}

	render()
	{
		if(this.arrData != "")
		{
			let elA = createElement("a", {}, this.arrData.substring(this.arrData.length - 4));
			elA.href = this.arrData;
			elA.style.color = "darkblue";
			elA.download = this.arrData.substring(this.arrData.lastIndexOf("/")+1);
			this.elParent.appendChild(elA);
		}
		else
		{
			this.elParent.appendChild(document.createTextNode("no file"));
		}
	}

	renderEdit()
	{
		this.elA = createElement("div", {}, "No file");
		this.elA.href = "#";

		let elInput = createElement(
			"input",
			{
				type : "file",
				style: "margin-top:7px;"
			}
		);
		this.elInput = elInput;

		elInput.addEventListener("change", () => {
			if(elInput.files[0])
			{
				let nFileSize = elInput.files[0].size;
				if(nFileSize >= nMaxFileSize)
				{
					elInput.value = "";
					alert("Maximum size of file is " + (nMaxFileSize/1024.0/1024.0).toFixed(2) + "MB. Your file has " + (nFileSize/1024.0/1024.0).toFixed(2) + "MB");
					return;
				}

				let regex = /^([a-zA-Z0-9\s_\\.\-:()&])+(.pdf|.doc|.docx|.ppt|.pptx|.xls|.xlsx|.pptm|.csv|.mp4)$/;
				if (!regex.test(elInput.files[0].name.toLowerCase()))
				{
					alert(elInput.files[0].name + " is not a valid document name!");
					elInput.value = "";
					return;
				}
				this.elA.innerText = elInput.files[0].name;
				this.strFile = {
					element: $(this.elInput).clone(),
					text: elInput.files[0].name
				};
			}
		});

		this.elParent.appendChild(this.elA);
		this.elParent.appendChild(elInput);
	}

	updateValue(arrData)
	{
		this.strFile = arrData;
		if (typeof arrData == 'string')
		{
			this.elInput.value = "";
			this.elA.innerText = arrData;
		}
		else
		{
			$(this.elInput).replaceWith(arrData.element);
			this.elInput = arrData.element.get(0);
			this.elA.innerText = arrData.text;
		}


		if(
			this.arrOptions["internalAttributes"] !== undefined
			&& this.arrOptions["internalAttributes"]["required"] !== undefined
			&& (
				!this.form.bEditMode
				|| this.strFile === ""
			)
		)
			this.elInput.required = true;
		else
			this.elInput.required = false;
	}

	getValue()
	{
		return this.strFile;
	}

	getTextValue()
	{
		return "";
	}

	getPreviewValue()
	{
		return this.getTextValue();
	}

	prepareSubmit()
	{
		if(this.elInput.files.length)
		{
			let formData = new FormData();
			formData.append('file', this.elInput.files[0], this.elInput.files[0].name);
			formData.append('q', '{"method":"file_upload","params":[]}');
			return $.ajax({
				url: "../../privateEndpoint",
				type: "POST",
				data: formData,
				processData: false,  // tell jQuery not to process the data
				contentType: false   // tell jQuery not to set contentType
			}).then((answer) =>
			{
				if(answer.result === undefined)
					console.log(answer);
				else
					console.log("Uploaded file "+this.strKey+": "+answer.result);

				return {
					"key": this.strKey,
					"value": answer.result
				};
			}, (error) =>
			{
				console.error(error);
			});
		}
	}

	static isRenderBefore()
	{
		return false;
	}

	static isRenderAfter()
	{
		return true;
	}
}