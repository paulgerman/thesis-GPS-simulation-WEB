class MultiImage extends BaseFormElement {
	/**
	 * @param {Element} elParent
	 * @param arrData
	 * @param arrOptions
	 * @param {Form} form
	 * @param {String|undefined} strKey
	 */
	constructor(elParent, arrData, arrOptions, form, strKey)
	{
		super(elParent, arrData, arrOptions, form, strKey);
		this.arrPendingFiles = [];
		this.arrExistingFiles = [];


		this.elImagesDiv = createElement("div", {style: "float:left; margin-right:10px;display: flex;  flex-flow: row wrap;	justify-content: flex-start; align-content: flex-start;"});
		this.elParent.appendChild(this.elImagesDiv);

		$(this.elImagesDiv).sortable({
			revert: true,
		});
		$(this.elImagesDiv).disableSelection();

	}

	render()
	{
		let arrImages;
		if(this.arrData.length)
			arrImages = this.arrData.split(",");
		else
			arrImages = [];


		let elContainer = createElement("div", {style: "text-align:center"});
		for (let i in arrImages)
		{
			let elDiv = document.createElement("div");
			elDiv.setAttribute("style", "background-color: grey; background-size: 20%; float:left; margin-right:2px; margin-bottom:3px;");

			let elImg = document.createElement("img");
			elImg.src = arrImages[i];
			elImg.setAttribute("style", "max-width:50px");

			elDiv.appendChild(elImg);

			elContainer.appendChild(elDiv);
			this.elParent.appendChild(elContainer);
			if(i > 20)
			{
				elImg.src = "img/etc.png";
				elDiv.style.backgroundColor = "transparent";
				break;
			}
		}

		if (arrImages.length === 0)
			this.elParent.appendChild(document.createTextNode("no image"));
	}

	renderEdit()
	{
		let elAddDiv = createElement("div", {style: "float: left; margin-right: 10px;"});
		this.elAddDiv = elAddDiv;
		let elDiv = document.createElement("div");
		elDiv.addEventListener("click", (e) =>
		{
			if (e.target === elDiv || e.target === elAddImg)
				$(this.elInput).trigger('click');
		});
		elDiv.setAttribute("style", "position:relative; background-color: grey; background-size: 20%; float:left;");

		let elAddImg = document.createElement("img");
		elAddImg.setAttribute("style", "max-width:160px; cursor:pointer;");
		elAddImg.src = "img/add_image.png";
		elAddImg.alt = "Add image";

		elDiv.appendChild(elAddImg);

		let elInput = createElement(
			"input",
			{
				type: "file",
				style: "position:absolute; top:0; width:100%; height:100%; opacity:0;",
				multiple: true
			}
		);
		this.elInput = elInput;

		elInput.addEventListener("change", () =>
		{
			const MAX_RUN = 5;

			let nRunningOperations = 0;
			let nQueue = MAX_RUN;

			if (elInput.files[0])
			{
				for (const i in elInput.files)
				{
					if (!elInput.files.hasOwnProperty(i))
						continue;

					let reader = new FileReader();

					let nFileSize = elInput.files[i].size;
					let maxSize;
					if (typeof nMaxImageSize !== 'undefined')
						maxSize = nMaxFileSize;
					else
						maxSize = nMaxImageSize;
					if (nFileSize >= maxSize)
					{
						alert("Maximum size of image is " + (maxSize / 1024.0 / 1024.0).toFixed(2) + "MB. Your image has " + (nFileSize / 1024.0 / 1024.0).toFixed(2) + "MB");
						continue;
					}

					let regex = /^([a-zA-Z0-9\s_\\.\-:()&])+(.jpg|.jpeg|.gif|.png)$/;
					if (!regex.test(elInput.files[i].name.toLowerCase()))
					{
						alert(elInput.files[i].name + " is not a valid image name!");
						continue;
					}
					this.arrPendingFiles.push(elInput.files[i]);
					let nr = this.arrPendingFiles.length - 1;

					if(this.arrPendingFiles.length + elInput.files.length < 40)
					{
						const file = elInput.files[i];
						const fnStartOperation = () =>
						{
							nRunningOperations++;
							nQueue--;

							const img = createElement("img");
							img.addEventListener('load', () =>
							{
								console.log("start");
								const canvas = document.createElement("canvas");
								const ctx = canvas.getContext("2d");

								let MAX_WIDTH = 200;
								let MAX_HEIGHT = 200;
								let width = img.width;
								let height = img.height;

								if (width > height)
								{
									if (width > MAX_WIDTH)
									{
										height *= MAX_WIDTH / width;
										width = MAX_WIDTH;
									}
								}
								else
								{
									if (height > MAX_HEIGHT)
									{
										width *= MAX_HEIGHT / height;
										height = MAX_HEIGHT;
									}
								}
								canvas.width = width;
								canvas.height = height;
								ctx.drawImage(img, 0, 0, width, height);

								canvas.toBlob((blob) =>
								{
									const objectURL = URL.createObjectURL(blob);
									this.addImage(objectURL, true, nr);
									nRunningOperations--;
								});
								URL.revokeObjectURL(img.src);
							});

							img.src = URL.createObjectURL(file);
						};

						if(nRunningOperations > MAX_RUN)
						{
							nQueue++;
							setTimeout(fnStartOperation, 200 * (nRunningOperations - MAX_RUN + nQueue));
						}
						else
							fnStartOperation();

						/*
						 const self = this;
						 EXIF.getData(elInput.files[i], () => {
						 let orientation = EXIF.getTag(this, "Orientation");
						 let can = document.createElement("canvas");
						 let ctx = can.getContext('2d');
						 let thisImage = new Image;
						 thisImage.onload = () => {
						 can.width  = thisImage.width;
						 can.height = thisImage.height;
						 ctx.save();
						 let width  = can.width;
						 let height = can.height;
						 if (orientation) {
						 if (orientation > 4) {
						 can.width  = height;
						 can.height = width;
						 }
						 switch (orientation) {
						 case 2: ctx.translate(width, 0);     ctx.scale(-1,1); break;
						 case 3: ctx.translate(width,height); ctx.rotate(Math.PI); break;
						 case 4: ctx.translate(0,height);     ctx.scale(1,-1); break;
						 case 5: ctx.rotate(0.5 * Math.PI);   ctx.scale(1,-1); break;
						 case 6: ctx.rotate(0.5 * Math.PI);   ctx.translate(0,-height); break;
						 case 7: ctx.rotate(0.5 * Math.PI);   ctx.translate(width,-height); ctx.scale(-1,1); break;
						 case 8: ctx.rotate(-0.5 * Math.PI);  ctx.translate(-width,0); break;
						 }
						 }

						 ctx.drawImage(thisImage,0,0);
						 ctx.restore();
						 const dataURL = can.toDataURL("image/jpeg");
						 self.addImage(dataURL, true, nr);

						 // at this point you can save the image away to your back-end using 'dataURL'
						 }

						 // now trigger the onload function by setting the src to your HTML5 file object (called 'file' here)
						 console.log(elInput.files[i]);
						 thisImage.src = URL.createObjectURL(elInput.files[i]);
						 });*/
					}
					else
					{
						this.addImage("img/dummy.png", true, nr);
					}
				}
			}
			elInput.value = [];
			this.updateRequired();
		});

		elDiv.appendChild(elInput);
		elAddDiv.appendChild(elDiv);

		this.elParent.appendChild(elAddDiv);
	}

	updateValue(arrData)
	{
		this.removeImages();

		let arrImages;
		if(arrData.length)
			arrImages = arrData.split(",");
		else
			arrImages = [];

		this.arrExistingFiles = arrImages;
		this.arrPendingFiles = [];

		for(let i in arrImages)
			this.addImage(arrImages[i]);

		this.updateRequired();
	}

	getValue()
	{
		let arrFinal = [];
		for(let i = 0; i < this.elImagesDiv.childNodes.length; i++)
		{
			let el = this.elImagesDiv.childNodes[i];
			if (!el.bBlob)
			{
				for (let j in this.arrExistingFiles)
					if (el.refered === this.arrExistingFiles[j])
					{
						arrFinal.push(this.arrExistingFiles[j]);
						break;
					}
			}
		}
		return arrFinal;
	}

	getTextValue()
	{
		return "";
	}

	getPreviewValue()
	{
		return this.getTextValue();
	}

	prepareSubmit()
	{
		if (this.arrPendingFiles.length)
		{
			let formData = new FormData();
			for(let i in this.arrPendingFiles)
			{
				formData.append('image[]', this.arrPendingFiles[i], this.arrPendingFiles[i].name);
			}
			formData.append('q', '{"method":"image_uploadMultiple","params":[]}');
			return $.ajax({
				url: "../../privateEndpoint",
				type: "POST",
				data: formData,
				processData: false,  // tell jQuery not to process the data
				contentType: false   // tell jQuery not to set contentType
			}).then((answer) =>
			{
				if (answer.result === undefined)
					console.log(answer);
				else
					console.log("Uploaded images " + this.strKey + ": " + answer.result);

				let arrFinal = [];

				for(let i = 0; i < this.elImagesDiv.childNodes.length; i++)
				{
					let el = this.elImagesDiv.childNodes[i];
					if(el.bBlob)
					{
						for(let j in this.arrPendingFiles)
							if(el.refered === this.arrPendingFiles[j])
							{
								arrFinal.push(answer.result[j]);
								break;
							}
					}
					else
					{
						for(let j in this.arrExistingFiles)
							if(el.refered === this.arrExistingFiles[j])
							{
								arrFinal.push(this.arrExistingFiles[j]);
								break;
							}
					}
				}
				console.log(arrFinal);
				return {
					"key": this.strKey,
					"value": arrFinal
				}
			}, (error) =>
			{
				console.error(error);
			});
		}
	}

	static isRenderBefore()
	{
		return false;
	}

	static isRenderAfter()
	{
		return true;
	}

	addImage(strImage, bBlob = false, nNr = -1)
	{
		let elImageContainerDiv = createElement("div", {style: "float:left; margin-right: 10px; margin-bottom: 5px;"});
		let elImg = document.createElement("img");

		if(bBlob)
		{
			elImageContainerDiv.refered = this.arrPendingFiles[nNr];
			elImageContainerDiv.bBlob = true;
			elImg.addEventListener("load", () => {
				URL.revokeObjectURL(strImage);
			});
		}
		else
		{
			elImageContainerDiv.refered = strImage;
			elImageContainerDiv.bBlob = false;
		}

		let elDiv = document.createElement("div");
		elDiv.setAttribute("style", "position:relative; background-color: grey; background-size: 20%; float:left;");
		elDiv.setAttribute("class", "image-container");

		elImg.setAttribute("style", "max-width:120px; cursor:pointer;");
		elImg.alt = "Image preview";
		elImg.src = strImage;


		elDiv.appendChild(elImg);

		let elButton = createElement(
			"button",
			{
				class: "btn-danger btn-sm image-delete",
				type: "button",
			},
			"x"
		);
		elButton.addEventListener("click", () =>
		{
			elImageContainerDiv.remove();
			if(bBlob)
				this.arrPendingFiles.splice(nNr, 1);
			else
			{
				let index = this.arrExistingFiles.indexOf(strImage);
				this.arrExistingFiles.splice(index, 1);
			}


			this.updateRequired();

			console.log(this.arrPendingFiles);
			console.log(this.arrExistingFiles);
		});

		elImageContainerDiv.appendChild(elDiv);
		elDiv.appendChild(elButton);

		this.elImagesDiv.appendChild(elImageContainerDiv);
	}

	removeImages()
	{
		let arrElementsToRemove = [];
		for(let i = 0; i < this.elImagesDiv.childNodes.length; i++)
			arrElementsToRemove.push(this.elImagesDiv.childNodes[i]);

		for(let i in arrElementsToRemove)
			this.elImagesDiv.removeChild(arrElementsToRemove[i]);
	}

	updateRequired()
	{

		if (
			this.arrOptions["internalAttributes"] !== undefined
			&& this.arrOptions["internalAttributes"]["required"] !== undefined
			&& this.arrPendingFiles.length === 0
			&& this.arrExistingFiles.length === 0
		)
			this.elInput.required = true;
		else
			this.elInput.required = false;
	}
}