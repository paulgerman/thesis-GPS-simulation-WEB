class BootstrapWYSIWYG extends BaseFormElement{
	render()
	{
		let nMaxSize = 100;
		let strText = this.arrData.replace(/<!--[\s\S]*?-->/g, "");
		strText = strText.replace(/(<([^>]+)>)/ig,"");
		if(strText.length > nMaxSize)
			this.elParent.innerHTML = strText.substr(0, nMaxSize)+"...";
		else
			this.elParent.innerHTML = strText;
	}

	renderEdit()
	{
		let fRandom = Math.floor((Math.random() * 99999999) + 1);
		let elDiv = createElement("div");
		elDiv.innerHTML = `
	<div class="btn-toolbar" data-role="editor-toolbar" id="editor-toolbar`+fRandom+`" data-target="#editor`+fRandom+`">
      <div class="btn-group">
        <a class="btn dropdown-toggle" data-trigger="hover" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="" data-original-title="Font"><i class="icon-font"></i><b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a data-edit="fontName Serif" style="font-family:'Serif'">Serif</a></li><li><a data-edit="fontName Sans" style="font-family:'Sans'">Sans</a></li><li><a data-edit="fontName Arial" style="font-family:'Arial'">Arial</a></li><li><a data-edit="fontName Arial Black" style="font-family:'Arial Black'">Arial Black</a></li><li><a data-edit="fontName Courier" style="font-family:'Courier'">Courier</a></li><li><a data-edit="fontName Courier New" style="font-family:'Courier New'">Courier New</a></li><li><a data-edit="fontName Comic Sans MS" style="font-family:'Comic Sans MS'">Comic Sans MS</a></li><li><a data-edit="fontName Helvetica" style="font-family:'Helvetica'">Helvetica</a></li><li><a data-edit="fontName Impact" style="font-family:'Impact'">Impact</a></li><li><a data-edit="fontName Lucida Grande" style="font-family:'Lucida Grande'">Lucida Grande</a></li><li><a data-edit="fontName Lucida Sans" style="font-family:'Lucida Sans'">Lucida Sans</a></li><li><a data-edit="fontName Tahoma" style="font-family:'Tahoma'">Tahoma</a></li><li><a data-edit="fontName Times" style="font-family:'Times'">Times</a></li><li><a data-edit="fontName Times New Roman" style="font-family:'Times New Roman'">Times New Roman</a></li><li><a data-edit="fontName Verdana" style="font-family:'Verdana'">Verdana</a></li></ul>
        </div>
      <div class="btn-group">
        <a class="btn dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font Size"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
          <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
          <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
          </ul>
      </div>
      <div class="btn-group">
          <a class="btn dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Color"><i class="icon-tint"></i>&nbsp;<b class="caret"></b></a>
          <ul class="dropdown-menu">
          <li><a class="btn" data-edit="foreColor red" title="Use Red Color" style="color:red;">Red</a></li>
          <li><a class="btn" data-edit="foreColor green" title="Use Green Color" style="color:green;">Green</a></li>
          <li><a class="btn" data-edit="foreColor blue" title="Use Blue Color" style="color:blue;">Blue</a></li>
          <li><a class="btn" data-edit="foreColor purple" title="Use Purple Color" style="color:purple;">Purple</a></li>
          <li><a class="btn" data-edit="foreColor black" title="Use Black Color" style="color:black;">Black</a></li>
          <li><a class="btn" data-edit="foreColor orange" title="Use Orange Color" style="color:orange;">Orange</a></li>
          <li><a class="btn" data-edit="foreColor yellow" title="Use Yellow Color" style="color:yellow;">Yellow</a></li>
          <li><a class="btn" data-edit="foreColor gold" title="Use Gold Color" style="color:gold;">Gold</a></li>
          </ul>
	  </div>
      <div class="btn-group">
          <a class="btn dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Table"><i class="icon-table"></i>&nbsp;<b class="caret"></b></a>
          <ul class="dropdown-menu" style="min-width:200px;">
          <li><a style="clear:none;" class="btn" data-edit="insertHTML <table style='width:100%;'><tr><td>Val1</td><td>Val2</td></tr></table>">1x2</a></li>
          <li><a style="clear:none;" class="btn" data-edit="insertHTML <table style='width:100%;'><tr><td>Val1</td><td>Val2</td></tr><tr><td>Val3</td><td>Val4</td></tr></table>">2x2</a></li>
          <li><a style="clear:none;" class="btn" data-edit="insertHTML <table style='width:100%;'><tr><td>Val1</td><td>Val2</td></tr><tr><td>Val3</td><td>Val4</td></tr><tr><td>Val5</td><td>Val6</td></tr></table>">3x2</a></li>
          <li><a style="clear:none;" class="btn" data-edit="insertHTML <table style='width:100%;'><tr><td>Val1</td><td>Val2</td><td>Val3</td></tr></table>">1x3</a></li>
          <li><a style="clear:none;" class="btn" data-edit="insertHTML <table style='width:100%;'><tr><td>Val1</td><td>Val2</td><td>Val3</td></tr><tr><td>Val4</td><td>Val5</td><td>Val6</td></tr></table>">2x3</a></li>
          <li><a style="clear:none;" class="btn" data-edit="insertHTML <table style='width:100%;'><tr><td>Val1</td><td>Val2</td><td>Val3</td></tr><tr><td>Val4</td><td>Val5</td><td>Val6</td></tr><tr><td>Val7</td><td>Val8</td><td>Val9</td></tr></table>">3x3</a></li>
          </ul>
	  </div>
      <div class="btn-group">
        <a class="btn" data-edit="bold" title="" data-original-title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
        <a class="btn" data-edit="italic" title="" data-original-title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
        <a class="btn" data-edit="strikethrough" title="" data-original-title="Strikethrough"><i class="icon-strikethrough"></i></a>
        <a class="btn" data-edit="underline" title="" data-original-title="Underline (Ctrl/Cmd+U)"><i class="icon-underline"></i></a>
      </div>
      <div class="btn-group">
        <a class="btn" data-edit="insertunorderedlist" title="" data-original-title="Bullet list"><i class="icon-list-ul"></i></a>
        <a class="btn" data-edit="insertorderedlist" title="" data-original-title="Number list"><i class="icon-list-ol"></i></a>
        <a class="btn" data-edit="outdent" title="" data-original-title="Reduce indent (Shift+Tab)"><i class="icon-indent-left"></i></a>
        <a class="btn" data-edit="indent" title="" data-original-title="Indent (Tab)"><i class="icon-indent-right"></i></a>
      </div>
      <div class="btn-group">
        <a class="btn" data-edit="justifyleft" title="" data-original-title="Align Left (Ctrl/Cmd+L)"><i class="icon-align-left"></i></a>
        <a class="btn" data-edit="justifycenter" title="" data-original-title="Center (Ctrl/Cmd+E)"><i class="icon-align-center"></i></a>
        <a class="btn" data-edit="justifyright" title="" data-original-title="Align Right (Ctrl/Cmd+R)"><i class="icon-align-right"></i></a>
        <a class="btn" data-edit="justifyfull" title="" data-original-title="Justify (Ctrl/Cmd+J)"><i class="icon-align-justify"></i></a>
      </div>
      <div class="btn-group">
		  <a class="btn dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Hyperlink"><i class="icon-link"></i></a>
		    <div class="dropdown-menu input-append">
			    <input class="span2" placeholder="URL" type="text" data-edit="createLink">
			    <button class="btn" type="button">Add</button>
        </div>
        <a class="btn" data-edit="unlink" title="" data-original-title="Remove Hyperlink"><i class="icon-cut"></i></a>
      </div>
      
      <div class="btn-group">
        <a class="btn" title="" id="pictureBtn" data-original-title="Insert picture (or just drag drop)"><i class="icon-picture"></i></a>
        <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" style="opacity: 0; position: absolute; top: 0px; left: -3.66211e-05px; width: 36px; height: 30px;">
      </div>
      <div class="btn-group">
        <a class="btn" data-edit="undo" title="" data-original-title="Undo (Ctrl/Cmd+Z)"><i class="icon-undo"></i></a>
        <a class="btn" data-edit="redo" title="" data-original-title="Redo (Ctrl/Cmd+Y)"><i class="icon-repeat"></i></a>
      </div>
      <input type="text" data-edit="inserttext" id="voiceBtn" x-webkit-speech="" style="display: none;">
      <div class="btn-group">
        <a class="btn" data-edit="fontName Helvetica&&justifyfull&&foreColor black&&fontSize 3" title="" data-original-title="Default formatting"><i class="icon-eraser"></i></a>
      </div>
    </div>`;

		this.elParent.appendChild(elDiv);
		this.elEditor = createElement("div", {class: "form-control", style:"color:black; width:100%; height:auto; overflow:auto;", id:"editor"+fRandom});
		this.elEditor.addEventListener('paste', this.handlePaste.bind(this));
		this.elParent.appendChild(this.elEditor);

		setTimeout(() => {
			this.WYSIWYG = $(this.elEditor).wysiwyg({
				toolbarSelector: "#editor-toolbar"+fRandom
			});
			$('a[title]').tooltip({container:'body'});
			setTimeout(() => {
				$(this.elEditor).wysiwygResize({
					selector: "td",
				}).webkitimageresize();
			}, 200);
		},50);
	}

	updateValue(arrData)
	{
		$(this.elEditor).html(arrData);
	}

	getValue()
	{
		return this.WYSIWYG.cleanHtml();
	}

	getTextValue()
	{
		return $(this.WYSIWYG.cleanHtml()).text();
	}

	getPreviewValue()
	{
		return this.getTextValue();
	}

	static isRenderBefore()
	{
		return false;
	}

	static isRenderAfter()
	{
		return true;
	}

	handlePaste(e) {
		var clipboardData, pastedData;

		// Stop data actually being pasted into div
		e.stopPropagation();
		e.preventDefault();

		// Get pasted data via clipboard API
		clipboardData = e.clipboardData || window.clipboardData;
		pastedData = clipboardData.getData('Text');

		// Do whatever with pasteddata
		//alert(pastedData);
		console.log(pastedData);
		//pastedData = (pastedData).replace(/font-family\:[^;]+;?|font-size\:[^;]+;?|line-height\:[^;]+;?/g, '');

		pastedData = pastedData.replace(/(?:\r\n|\r|\n)/g, '<br />');

		pastedData = `
<div style="text-align: justify;">
	<span style="font-family: Helvetica;">
		<font size="3">${pastedData}</font>
	</span>
</div>`
		document.execCommand("insertHTML", false, pastedData);
	}
}