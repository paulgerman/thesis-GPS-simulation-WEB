class Image extends BaseFormElement{
	/**
	 * @param {Element} elParent
	 * @param arrData
	 * @param arrOptions
	 * @param {Form} form
	 * @param {String|undefined} strKey
	 */
	constructor(elParent, arrData, arrOptions, form, strKey)
	{
		super(elParent, arrData, arrOptions, form, strKey);
		this.strFile = "";
	}

	render()
	{
		if(this.arrData !== "")
		{
			let elDiv = document.createElement("div");
			elDiv.setAttribute("style", "background-color: grey; background-size: 20%; display: inline-block;");

			let elImg = document.createElement("img");
			elImg.src = this.arrData;
			elImg.setAttribute("style", "max-width:50px");

			elDiv.appendChild(elImg);

			this.elParent.appendChild(elDiv);
		}
		else
		{
			this.elParent.appendChild(document.createTextNode("no image"));
		}
	}

	renderEdit()
	{
		let elDiv = document.createElement("div");
		elDiv.addEventListener("click", (e) => {
			if(e.target == elDiv || e.target == elImg)
				$(this.elInput).trigger('click');
		});
		elDiv.setAttribute("style", "position:relative; background-color: grey; background-size: 20%; display: inline-block;");

		let elImg = document.createElement("img");
		elImg.setAttribute("style", "max-width:160px; cursor:pointer;");
		elImg.src = "img/add_image.png";
		elImg.alt = "Image preview";

		this.elImg = elImg;

		elDiv.appendChild(elImg);

		let elButton = createElement(
			"button",
			{
				class : "btn-danger btn-sm",
				style : "display: none",
				type : "button"
			},
			"x"
		);
		elButton.addEventListener("click", () => {
			this.strFile = "";
			this.elImg.src = "img/add_image.png";
			this.elInput.value = "";
			elButton.style.display = "none";
			if(this.arrOptions["internalAttributes"] !== undefined && this.arrOptions["internalAttributes"]["required"] !== undefined)
				this.elInput.required = true;
		});

		this.elButton = elButton;

		let elInput = createElement(
			"input",
			{
				type : "file",
				style: "position:absolute; top:0; width:100%; height:100%; opacity:0;"
			}
		);
		this.elInput = elInput;

		elInput.addEventListener("change", () => {
			let reader  = new FileReader();
			reader.addEventListener("load", () => {
				elImg.src = reader.result;
			}, false);

			if(elInput.files[0])
			{
				let nFileSize = elInput.files[0].size;
				let maxSize;
				if(typeof nMaxImageSize !== 'undefined')
					maxSize = nMaxFileSize;
				else
					maxSize = nMaxImageSize;
				if(nFileSize >= maxSize)
				{
					elInput.value = "";
					alert("Maximum size of image is " + (maxSize/1024.0/1024.0).toFixed(2) + "MB. Your image has " + (nFileSize/1024.0/1024.0).toFixed(2) + "MB");
					return;
				}

				let regex = /^([a-zA-Z0-9\s_\\.\-:()&])+(.jpg|.jpeg|.gif|.png)$/;
				if (!regex.test(elInput.files[0].name.toLowerCase()))
				{
					alert(elInput.files[0].name + " is not a valid image name!");
					elInput.value = "";
					return;
				}
				this.strFile = {
					element: $(this.elInput).clone(),
					text: elInput.files[0].name
				};

				reader.readAsDataURL(elInput.files[0]);
				this.elButton.style.display = "inline-block";
			}
		});

		elDiv.appendChild(elInput);
		this.elParent.appendChild(elDiv);
		this.elParent.appendChild(elButton);
	}

	updateValue(arrData)
	{
		this.strFile = arrData;

		if (typeof arrData == 'string')
		{
			this.elInput.value = "";
			if(arrData !== '')
				this.elImg.src = arrData;
			else
				this.elImg.src = 'img/add_image.png';
		}
		else
		{
			$(this.elInput).replaceWith(arrData.element);
			this.elInput = arrData.element.get(0);
			let reader  = new FileReader();
			reader.addEventListener("load", () => {
				this.elImg.src = reader.result;
			}, false);
			reader.readAsDataURL(this.elInput.files[0]);
		}


		if(this.strFile !== "")
		{
			this.elButton.style.display = "inline-block";
		}
		else
		{
			this.elButton.style.display = "none";
		}

		if(
			this.arrOptions["internalAttributes"] !== undefined
			&& this.arrOptions["internalAttributes"]["required"] !== undefined
			&& (
				!this.form.bEditMode
				|| this.strFile === ""
			)
		)
			this.elInput.required = true;
		else
			this.elInput.required = false;
	}

	getValue()
	{
		return this.strFile;
	}

	getTextValue()
	{
		return "";
	}

	getPreviewValue()
	{
		return this.getTextValue();
	}

	prepareSubmit()
	{
		if(this.elInput.files.length)
		{
			let formData = new FormData();
			formData.append('image', this.elInput.files[0], this.elInput.files[0].name);
			formData.append('q', '{"method":"image_upload","params":[]}');
			return $.ajax({
				url: "../../thesisEndpoint",
				type: "POST",
				data: formData,
				processData: false,  // tell jQuery not to process the data
				contentType: false   // tell jQuery not to set contentType
			}).then((answer) =>
			{
				if(answer.result === undefined)
					console.log(answer);
				else
					console.log("Uploaded image "+this.strKey+": "+answer.result);
				return {
					"key": this.strKey,
					"value": answer.result
				};
			}, (error) =>
			{
				console.error(error);
			});
		}
	}

	static isRenderBefore()
	{
		return false;
	}

	static isRenderAfter()
	{
		return true;
	}
}