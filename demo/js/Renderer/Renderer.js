class Renderer {
	constructor(elParent, arrConfig, strAPINamespace) {
		this.elParent = elParent;
		this.arrConfig = arrConfig;
		this.arrElements = [];
		this.strAPINamespace = strAPINamespace;
	}


	static _getSelectableItems(arrItemOptions)
	{
		let arrItems = {};

		for(let i in arrItemOptions)
		{
			if(array_key_exists("type", arrItemOptions[i]) && arrItemOptions[i]["type"] === "optgroup")
			{
				$.extend(arrItems, Renderer._getSelectableItems(arrItemOptions[i]["data"]));
			}
			else
				arrItems[arrItemOptions[i]["value"]] = arrItemOptions[i];
		}

		return arrItems;
	}


}