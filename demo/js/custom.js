
function remove_image(elTrigger, strID, strPrefix)
{
	if(document.getElementById(strPrefix + strID + "_delete") != null)
		return;

	var x = document.getElementById(strPrefix + strID);

	if (x.type == "file")
	{
		var newItem = document.createElement("input");
		newItem.type = "hidden";
		newItem.name = strID + "_delete";
		newItem.id = strPrefix + strID + "_delete";
		elTrigger.parentNode.insertBefore(newItem, elTrigger);
	}

	x = document.getElementById(strPrefix + strID + "_show");
	x.style.display = 'none';

	elTrigger.parentNode.removeChild(elTrigger);
}

function updateImage(strID)
{
	if(typeof(FileReader)!="undefined")
	{
		var $img = $("#" + strID + "_show");
		var $input = $("#"+strID);

		var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png)$/;
		$($input[0].files).each(function ()
		{
			var oldSrc = $img.attr("src");
			$img.attr("src", "img/loading.gif");
			var bMadeVisible = false
			if(!$img.is(":visible"))
			{
				bMadeVisible = true;
				$img.show();
			}

			var getfile = $(this);
			if (regex.test(getfile[0].name.toLowerCase()))
			{
				var reader = new FileReader();
				reader.onload = function (e) {
					$img.attr("src",e.target.result);
				};
				reader.readAsDataURL(getfile[0]);
			}
			else
			{
				alert(getfile[0].name + " is not a valid image.");
				$img.attr("src", oldSrc);
				if(bMadeVisible)
					$img.hide();
				$input.val('');

				return false;
			}
		});
	}
}

function checkFileSize(strID, nMaxFileSize)
{
	var elInput = document.getElementById(strID);
	elInput.onchange = function(){
		var filesize = elInput.files[0].size;
		if(filesize >= nMaxFileSize)
		{
			elInput.value = "";
			alert("Maximum size of file is " + (nMaxFileSize/1024.0/1024.0).toFixed(2) + "MB. Your file has " + (filesize/1024.0/1024.0).toFixed(2) + "MB");
		}
	}
}

function trickyColor(strID)
{
	var elDivs = document.getElementsByClassName(strID);
	var i;
	for (i = 0; i < elDivs.length; i++) {
		var color = "default";

		switch(elDivs[i].innerHTML)
		{
			case "red":
				color = '#FF9999';
				break;
			case "yellow":
				color = '#ffffb3';
				break;
			case "orange":
				color = '#ffcc99';
				break;
			case "green":
				color = '#80ffbf';
				break;
		}
		if (color != "default" && elDivs[i].parentNode.parentNode.nodeName == "TR")
			elDivs[i].parentNode.parentNode.style.backgroundColor = color;
	}
}


function call(strMethodName, arrParams, fnSuccessCallback, fnErrorCallBack){
	$.jsonRPC.setup({
		endPoint: '../../privateEndpoint'
	});

	console.log('Called ' + strMethodName + " with params ");
	console.log(JSON.stringify(arrParams));

	$.jsonRPC.request(strMethodName, {
		params: arrParams,
		success: (result) => {
			console.log('Result:');
			console.log(result);
			if(fnSuccessCallback !== undefined)
				fnSuccessCallback(result["result"]);
		},
		error: (result) => {
			console.error("Error " + result.error.code + ": " + result.error.message);
			if(fnErrorCallBack !== undefined)
				fnErrorCallBack(result.error);
		}
	});
}

function batchCall(arrRequests, fnSuccessCallback, fnErrorCallBack){
	$.jsonRPC.setup({
		endPoint: '../../privateEndpoint'
	});
	console.log(arrRequests);
	$.jsonRPC.batchRequest(arrRequests, {
		success: (result) => {
			console.log('Batch called');
			console.log(arrRequests);
			console.log('Result:');
			console.log(result);
			if(fnSuccessCallback !== undefined)
				fnSuccessCallback(result["result"]);
		},
		error: (result) => {
			console.error("Error " + result.error.code + ": " + result.error.message);
			if(fnErrorCallBack !== undefined)
				fnErrorCallBack();
		}
	});
}

var done = 0;
var initialValue = null;

function changeSelect(obj)
{
	var dataToAdd = $(obj).val();
	var select2 = $("#ingredientitem_parents_type");


	if(done === 0 && select2.val() != null)
	{
		done = 1;
		initialValue = select2.val();
		dataToAdd = dataTypes[select2.val()[0]]["item_parents_category"][0];
		$(obj).val([dataTypes[select2.val()[0]]["item_parents_category"][0]]).change();
	}

	select2.empty();
	for(var i in dataCategoryTypes[dataToAdd])
	{
		select2.append('<option value="'+ dataCategoryTypes[dataToAdd][i]["item_id"] +'">'+ dataCategoryTypes[dataToAdd][i]["item_name"] +'</option>');
	}

	if(initialValue !== null)
	{
		select2.val([initialValue]);
	}

	select2.select2({
		placeholder: "Select type",
		maximumSelectionLength: 1
	});
}

function parseParms(str)
{
	var pieces = str.split("&"), data = {}, i, parts;
	// process each query pair
	for (i = 0; i < pieces.length; i++) {
		parts = pieces[i].split("=");
		if (parts.length < 2) {
			parts.push("");
		}
		data[decodeURIComponent(parts[0])] = decodeURIComponent(parts[1]);
	}
	return data;
}

function getQueryVariable(variable)
{
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		if(pair[0] == variable){return pair[1];}
	}
	return(false);
}