<?php
namespace Controllers
{
	abstract class BaseController
	{
		public $strClassName;

		abstract function __construct();

		function create($arrData)
		{
			$strClassName = $this->strClassName;
			$strClassName::remove_read_only_props($arrData);
			return $this->createInternal($arrData);
		}

		function createInternal($arrData)
		{
			$strClassName = $this->strClassName;
			$newObject = new $strClassName($arrData);
			trdb()->exec(
				"INSERT INTO `".$strClassName::$strTableName."`
					".$newObject->build_insert_query()
			);

			$newObject->arrValues[$strClassName::$strIndexProp] = trdb()->lastInsertId();
			return $this->get($newObject->arrValues[$strClassName::$strIndexProp]);
		}

		function delete($strID)
		{
			$strClassName = $this->strClassName;
			$nRowsAffected = trdb()->exec("DELETE FROM `".$strClassName::$strTableName."` WHERE `".$strClassName::$strIndexProp."` = ".(int)$strID);

			if($nRowsAffected != 1)
			{
				throw new \GeneralException($strClassName." with id ".(int)$strID." not found!", \GeneralException::PRODUCT_NOT_FOUND);
			}
			return true;
		}

		function delete_all($strCondition = "")
		{
			$strClassName = $this->strClassName;
			trdb()->exec("DELETE FROM `".$strClassName::$strTableName."` ".$strCondition);
		}

		function get($strValue, $strColumn = "")
		{
			$strClassName = $this->strClassName;
			if($strColumn == "")
				$strColumn = $strClassName::$strIndexProp;
			$object = null;
			$result = trdb()->query("SELECT * FROM `".$strClassName::$strTableName."` WHERE `".$strColumn."` = ".trdb()->quote($strValue));
			while($row = $result->fetchAll(\PDO::FETCH_ASSOC))
			{
				$object = new $strClassName($row[0]);
			}
			if($object == null)
			{
				throw new \GeneralException($strClassName." with ".$strColumn." = ".$strValue." not found!", \GeneralException::PRODUCT_NOT_FOUND);
			}
			return $object->arrValues;
		}

		function edit($strID, $arrData)
		{
			$strClassName = $this->strClassName;
			$strClassName::remove_read_only_props($arrData);
			return $this->editInternal($strID, $arrData);
		}

		function editInternal($strID, $arrData)
		{
			$pdo = trdb();
			$strClassName = $this->strClassName;

			$strQueryPart = "";
			foreach($strClassName::$arrProps as $strProp)
			{
				if(array_key_exists($strProp, $arrData) && $strProp != $strClassName::$strIndexProp)
				{
					$strQueryPart .= "`$strProp` = ".$pdo->quote(is_array($arrData[$strProp])?implode(",",$arrData[$strProp]):$arrData[$strProp]).", ";
				}
			}
			$strQueryPart = substr($strQueryPart, 0, -2);
			if($strQueryPart != "")
			{
				trdb()->exec("
					UPDATE `".$strClassName::$strTableName."` SET
					".$strQueryPart."
					WHERE `".$strClassName::$strIndexProp."` = ".$pdo->quote($strID)."
				");
				return $this->get($strID);
			}
			else
			{
				throw new \GeneralException("No properties given to update");
			}
		}

		function get_all($nStart = 0, $nLimit = 10, $arrFilter = array(), $strSortCol = [], $strOrder = [])
		{
			return $this->get_all_internal($nStart, $nLimit, static::generateFilter($arrFilter), $strSortCol, $strOrder);
		}

		function get_all_internal($nStart = 0, $nLimit = 10, $strFilter = "", $arrSortCols = [], $arrOrder = [])
		{
			$strClassName = $this->strClassName;
			$arrObjects = array();

			$strQuery = "
				SELECT * FROM
				 `".$strClassName::$strTableName."`".$strFilter." ";

			//backwards compatibility
			if(is_string($arrSortCols))
			{
				$arrSortCols = array($arrSortCols);
				if(!is_array($arrOrder))
					$arrOrder = array($arrOrder);
			}

			if(count($arrSortCols) > count($arrOrder))
			{
				for($i = 0; $i < count($arrSortCols) - count($arrOrder); $i++)
					$arrOrder[] = "ASC";
			}

			if(count($arrSortCols))
			{
				$strSort = "";

				if(!in_array($strClassName::$strIndexProp, $arrSortCols))
				{
					$arrSortCols[] = $strClassName::$strIndexProp;
					$arrOrder[] = "ASC";
				}
				foreach($arrSortCols as $nKey => $strSortCol)
				{
					$strOrder = strtoupper($arrOrder[$nKey]);
					if($strOrder != "ASC" && $strOrder != "DESC")
						throw new \GeneralException("Don't do this sir :(");
					$strSort .= "`".$strSortCol."` ".$strOrder.", ";
				}
				$strSort = substr($strSort, 0, -2);
				$strQuery .= "ORDER BY ".$strSort." ";
			}
			$strQuery .= "LIMIT ".(int)$nStart.", ".(int)$nLimit;
			$arrResults = trdb()->query($strQuery)->fetchAll(\PDO::FETCH_ASSOC);
			foreach($arrResults as $arrResult)
			{
				$object = new $strClassName($arrResult);
				$arrObjects[$object->arrValues[$strClassName::$strIndexProp]] = $object->arrValues;
			}
			return $arrObjects;
		}

		function get_unique_values($arrKeys)
		{
			$strClassName = $this->strClassName;
			$arrResult = [];
			foreach($arrKeys as $strKey)
			{
				$result = trdb()->query("SELECT DISTINCT `$strKey` FROM `".$strClassName::$strTableName."`");
				$arrResult[$strKey] = [];
				while($row = $result->fetch(\PDO::FETCH_ASSOC))
				{
					$arrResult[$strKey][] = $row[$strKey];
				}
			}
			return $arrResult;
		}

		function processFile(&$arrData, $strKey, $strType)
		{
			$strClassName = $this->strClassName;
			$bOK = false;
			if(array_key_exists($strKey, $arrData))
			{
				$bUnset = false;
				if(is_array($arrData[$strKey]))
				{
					$strFileType = strtolower(pathinfo($arrData[$strKey]["name"], PATHINFO_EXTENSION));
					if(is_array($arrData[$strKey]) && $arrData[$strKey]["error"] != 4)
					{
						if($arrData[$strKey]["error"] == 0)
						{
							$strFileName = pathinfo($arrData[$strKey]["name"], PATHINFO_FILENAME);
						}
						else
						{
							throw new \GeneralException("Upload error: ".$arrData[$strKey]["error"]);
						}
					}
					else
						$bUnset = true;
				}
				else
				{
					return;
					if($arrData[$strKey] === "")
						return;

					$nPosStart = strpos($arrData[$strKey], ":");
					$nPosEnd = strpos($arrData[$strKey], ";");
					if($nPosStart === false || $nPosEnd === false)
						$bUnset = true;

					$strFileType = substr($arrData[$strKey], $nPosStart + 1, $nPosEnd - $nPosStart - 1);

					$arrExtensions = array(
						'image/jpeg' => "jpeg",
						'image/png' => "png",
						'image/jpg' => "jpg",
						'image/gif' => "gif",
						'application/pdf' => "pdf",
						'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'docx',
						'application/msword' => "doc",
						'application/mspowerpoint' => 'ppt',
						'application/powerpoint' => 'ppt',
						'application/vnd.ms-powerpoint' => 'ppt',
						'application/x-mspowerpoint' => 'ppt',
						'application/vnd.ms-excel' => 'xls',
						'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'xlsx',
						'application/vnd.openxmlformats-officedocument.spreadsheetml.template' => 'xltx',
						'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'pptx',
						'application/vnd.openxmlformats-officedocument.presentationml.template' => 'potx',
						'application/vnd.openxmlformats-officedocument.presentationml.slideshow' => 'ppsx',
						'application/vnd.ms-powerpoint.presentation.macroEnabled.12' => 'pptm',
					);
					if(array_key_exists($strFileType, $arrExtensions))
						$strFileType = $arrExtensions[$strFileType];

					$strFileName = "json";
				}

				if($bUnset)
				{
					unset($arrData[$strKey]);

					if(array_key_exists($strKey."_delete", $arrData))
					{
						$arrData[$strKey] = "";
					}
					return;
				}

				if($strType == "image")
				{
					$arrAcceptedFormats = array(
						'gif', 'png', 'jpg', 'jpeg'
					);
					$strDirectoryRelativeToRoot = "uploadedImages";

				}
				elseif($strType == "file")
				{
					$arrAcceptedFormats = array();
					$strDirectoryRelativeToRoot = "uploadedFiles";
				}
				else
				{
					throw new \GeneralException("Invalid TYPE");
				}

				if(!in_array($strFileType, $arrAcceptedFormats) && !empty($arrAcceptedFormats))
				{
					throw new \GeneralException("Invalid file type ".$strFileType);
				}

				$strRelativePath = "/".$strDirectoryRelativeToRoot."/".uniqid($strFileName."-").".".$strFileType;
				$strTargetFile = $_SERVER["DOCUMENT_ROOT"].$strRelativePath;

				//image resize if needed
				if($strType == "image")
				{
					if(isset($strClassName::$arrRenderProps[$strKey]) && isset($strClassName::$arrRenderProps[$strKey]["maxWidthOrHeight"]))
						$maxDim = $strClassName::$arrRenderProps[$strKey]["maxWidthOrHeight"];
					else
						$maxDim = 3086;

					if(is_array($arrData[$strKey]))
					{
						if($strFileType == "png")
							$img = @imagecreatefrompng($arrData[$strKey]["tmp_name"]);
						elseif($strFileType == "jpg" || $strFileType == "jpeg")
							$img = @imagecreatefromjpeg($arrData[$strKey]["tmp_name"]);
						elseif($strFileType == "gif")
							$img = @imagecreatefromgif($arrData[$strKey]["tmp_name"]);
						else
							throw new \GeneralException("Err");
					}
					else
					{
						$blobImg = base64_decode(substr($arrData[$strKey], strpos($arrData[$strKey], ",") + 1));
						$img = @imagecreatefromstring($blobImg);
					}

					if($img === false)
					{
						throw new \GeneralException("Bad image");
					}

					$nWidthOrg = $nWidth = imagesx($img);
					$nHeightOrg = $nHeight = imagesy($img);


					if($nWidth > $maxDim || $nHeight > $maxDim)
					{
						$ratio = $nWidth / $nHeight; // width/height
						if($ratio > 1)
						{
							$nWidth = $maxDim;
							$nHeight = $maxDim / $ratio;
						}
						else
						{
							$nWidth = $maxDim * $ratio;
							$nHeight = $maxDim;
						}
						$dst = imagecreatetruecolor($nWidth, $nHeight);

						imagealphablending($dst, false);
						imagesavealpha($dst, true);

						imagecopyresampled($dst, $img, 0, 0, 0, 0, $nWidth, $nHeight, $nWidthOrg, $nHeightOrg);
						imagedestroy($img);
					}
					else
						$dst = $img;


					$exif = @exif_read_data($arrData[$strKey]["tmp_name"]);
					if (!empty($exif['Orientation'])) {
						switch ($exif['Orientation']) {
							case 3:
								$dst = imagerotate($dst, 180, 0);
								break;

							case 6:
								$dst = imagerotate($dst, -90, 0);
								break;

							case 8:
								$dst = imagerotate($dst, 90, 0);
								break;
						}
					}

					if($strFileType == "png")
						$res = imagepng($dst, $strTargetFile);
					elseif($strFileType == "jpg" || $strFileType == "jpeg")
						$res = imagejpeg($dst, $strTargetFile);
					elseif($strFileType == "gif")
						$res = imagegif($dst, $strTargetFile);
					else
						throw new \GeneralException("ERR!");
					if(!$res)
						throw new \GeneralException("Error creating image");
					imagedestroy($dst);
				}


				if($strType !== "image")
				{
					if(is_array($arrData[$strKey]))
					{
						if(!move_uploaded_file($arrData[$strKey]["tmp_name"], $strTargetFile))
						{
							throw new \GeneralException("Can't upload file");
						}
					}
					else
					{
						if(file_put_contents($strTargetFile, base64_decode(substr($arrData[$strKey], strpos($arrData[$strKey], ",") + 1))) === false)
						{
							throw new \GeneralException("Error uploading file!");
						}
					}
				}

				$strSHAFileName = sha1_file($strTargetFile).".".$strFileType;
				$strRelativePathSHA = "/".$strDirectoryRelativeToRoot."/".$strSHAFileName;
				$strTargetFileSHA = $_SERVER["DOCUMENT_ROOT"].$strRelativePathSHA;

				if(file_exists($strSHAFileName))
					unlink($strSHAFileName);
				rename($strTargetFile, $strTargetFileSHA);

				$arrData[$strKey] = $strRelativePathSHA;
			}
		}

		/*
		 * [
				"col" => "id_a",
				"op" => ">",
				"val" => 23
			]
		 */
		static function generateFilter($arrFilter)
		{
			if(!is_array($arrFilter))
				throw new \GeneralException("Invalid usage of filter");
			if(empty($arrFilter))
				return "";
			if(isset($arrFilter["AND"]))
				return " WHERE ".static::generateFilterRec($arrFilter["AND"], "AND");
			elseif(isset($arrFilter["OR"]))
				return " WHERE ".static::generateFilterRec($arrFilter["OR"], "OR");
			else
				return " WHERE ".static::generateFilterRec($arrFilter, "");
		}

		static function generateFilterRec($arrFilter, $strMainCond)
		{
			$pdo = trdb();
			if($strMainCond !== "AND" && $strMainCond !== "OR")
			{
				if(array_keys($arrFilter) != array('col', 'op', 'val'))
					throw new \GeneralException("Invalid filter data: ".json_encode($arrFilter));

				if(!preg_match('/^[a-zA-Z_][a-zA-Z0-9_]*$/', $arrFilter["col"]))
					throw new \GeneralException("Invalid column name ".$arrFilter["col"]);
				if(!in_array($arrFilter["op"], array('>', '>=', '<', '<=', '=')))
					throw new \GeneralException("Invalid operator ".$arrFilter["op"]);
				if(is_array($arrFilter["val"]))
					throw new \GeneralException("Value can't be array");
				return '`'.$arrFilter["col"].'` '.$arrFilter["op"].' '.$pdo->quote($arrFilter["val"]);
			}
			else
			{
				$strFilter = "(";
				foreach($arrFilter as $arrValue)
				{
					if(isset($arrValue["AND"]))
						$strFilter .= static::generateFilterRec($arrValue["AND"], "AND").' '.$strMainCond.' ';
					elseif(isset($arrValue["OR"]))
						$strFilter .= static::generateFilterRec($arrValue["OR"], "OR").' '.$strMainCond.' ';
					else
						$strFilter .= static::generateFilterRec($arrValue, "").' '.$strMainCond.' ';
				}
				$strFilter = substr($strFilter, 0, -(strlen($strMainCond) + 2));
				$strFilter .= ")";

				return $strFilter;
			}
		}

		function search($strQuery, $arrColumns)
		{
			$strClassName = $this->strClassName;
			$pdo = trdb();
			$arrObjects = array();

			$strSubQuery = " `".$arrColumns[0]."` LIKE ".$pdo->quote("%".$strQuery."%");
			for($i = 1; $i < count($arrColumns); $i++)
				$strSubQuery .= " OR `".$arrColumns[$i]."` LIKE '%".$strQuery."%'";

			$arrResults = $pdo->query("SELECT * FROM `".$strClassName::$strTableName."` WHERE $strSubQuery")->fetchAll(\PDO::FETCH_ASSOC);
			foreach($arrResults as $arrResult)
			{
				$object = new $strClassName($arrResult);
				$arrObjects[$object->arrValues[$strClassName::$strIndexProp]] = $object->arrValues;
			}
			return $arrObjects;
		}

		function count_internal($strCondition = "")
		{
			$strClassName = $this->strClassName;

			$strQuery = "
				SELECT count(".$strClassName::$strIndexProp.") FROM
				 `".$strClassName::$strTableName."`".$strCondition." ";

			$strResult = trdb()->query($strQuery)->fetchColumn();
			return $strResult;
		}
	}
}