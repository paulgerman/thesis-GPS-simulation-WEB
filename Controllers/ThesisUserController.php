<?php

namespace Controllers;


class ThesisUserController extends BaseController
{
	function __construct()
	{
		$this->strClassName = \Objects\ThesisUser::class;
	}

	function create($arrData)
	{
		$arrData["user_salt"] = mt_rand();
		if($arrData["user_password"] != "")
			$arrData["user_password"] = md5($arrData["user_password"].$arrData["user_salt"]);
		else
			$arrData["user_password"] = md5("test".$arrData["user_salt"]);
		return parent::create($arrData);
	}

	function edit($strID, $arrData)
	{
		if($arrData["user_password"] != "")
		{
			$arrUser = $this->get($strID);
			$arrData["user_password"] = md5($arrData["user_password"].$arrUser["user_salt"]);
		}
		else
			unset($arrData["user_password"]);

		return parent::edit($strID, $arrData);
	}
}