<?php

namespace classes\JSONRPC\Endpoints;
use classes\JSONRPC\Server;

class ThesisEndpoint extends Server
{
	function allowedFunctions()
	{
		return array(
			"user" => \Controllers\ThesisUserController::class,
			"userSession" => \Controllers\ThesisUserSessionController::class,
			"track" => \Controllers\ThesisTrackController::class,
			"simulation" => \Controllers\ThesisSimulationController::class,
			"image" => \Controllers\ImageController::class,
		);
	}

	function addPlugins()
	{
		/*$this->addPlugin(new \classes\JSONRPC\Plugins\AuthenticationPlugin(function($strKey){
			return true;
		}));*/
		$this->addPlugin(new \classes\JSONRPC\Plugins\FilterPlugin(array(
			"user_password",
			"user_salt"
		)));
	}
}

$strClassName = '\\classes\\JSONRPC\\Endpoints\\'.pathinfo(__FILE__, PATHINFO_FILENAME);
/** @var Server $jsonRPC*/
$jsonRPC = new $strClassName("logs/");
$jsonRPC->start();
