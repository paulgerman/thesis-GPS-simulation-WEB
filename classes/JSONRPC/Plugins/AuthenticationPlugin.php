<?php
namespace classes\JSONRPC\Plugins;


use classes\JSONRPC\JSONRPCException;

class AuthenticationPlugin implements PluginBase
{
	private $fnValidate;
	public function __construct(callable $fnValidate = null)
	{
		$this->fnValidate = $fnValidate;
	}

	function beforeProcess($arrData)
	{
		$bAuthenticated = false;
		if(array_key_exists("auth", $_COOKIE))
		{
			if($this->fnValidate !== null)
			{
				$fn = $this->fnValidate;
				$bAuthenticated = $fn($_COOKIE["auth"]);
			}
			else
				$bAuthenticated = $this->_authTest($_COOKIE["auth"]);
		}
		else
		{
			throw new JSONRPCException("To use this endpoint you need to use authentication!");
		}

		if(!$bAuthenticated)
		{
			throw new JSONRPCException("Authentication failed!");
		}

		return $arrData;
	}

	function afterProcess($arrData)
	{
		return $arrData;
	}


	private function _authTest($strToken)
	{
		if($strToken == "1235")
			return true;
		return false;
	}
}