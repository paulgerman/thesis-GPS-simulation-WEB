<?php

namespace classes\JSONRPC\Plugins;


interface PluginBase
{
	public function beforeProcess($arrData);

	public function afterProcess($arrData);
}